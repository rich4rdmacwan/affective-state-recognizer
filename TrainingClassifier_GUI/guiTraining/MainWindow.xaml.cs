﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Windows.Threading;

namespace guiTraining
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class Global
        {
            public static string logsDir = "";
            public static string modelDir = "";
        }
        public MainWindow()
        {
            
            InitializeComponent();
            comboBoxClassifier.SelectedIndex = 0;
        }
        public void appendFiles(string[] csvFiles)
        {
            txtViewer.Text +="Starting the appending..." + System.Environment.NewLine;
            //--Creating the training file
            FileStream savedFile = File.Create(Global.logsDir + "\\training.csv");
            string output = "";
            StreamWriter wrt = new StreamWriter(savedFile);

            //--Appending the data
            for (int i = 0; i < csvFiles.Length; i++)
            {
                //Open the file
                FileStream file = File.OpenRead(csvFiles[i]);
                StreamReader rdr = new StreamReader(file);

                if (i != 0)
                {
                    //Read (ommite) first line
                    rdr.ReadLine();
                }

                //Read to end
                var data = rdr.ReadToEnd();
                // Appending all the data to the training file
                output += data;
                rdr.Close();
                file.Close();
            }
            wrt.Write(output);
            wrt.Close();
            txtViewer.Text +="Appending finished" + System.Environment.NewLine;
        }
        public void csv2arff()
        {
            txtViewer.Text +="Converting the CSV file to ARFF..." + System.Environment.NewLine;
            // Load CSV
            weka.core.converters.ConverterUtils.DataSource source = new weka.core.converters.ConverterUtils.DataSource(Global.logsDir + "\\training.csv");
            weka.core.Instances data = source.getDataSet();

            // Save ARFF
            weka.core.converters.ArffSaver saver = new weka.core.converters.ArffSaver();
            saver.setInstances(data);
            saver.setFile(new java.io.File(Global.logsDir + "\\training.arff"));
            saver.writeBatch();

            // Changing the numeric value of the classLabel to nominal values
            StreamReader rdr = new StreamReader(Global.logsDir + "\\training.arff");
            string fileData = rdr.ReadToEnd();
            rdr.Close();
            string replacedData = fileData.Replace("@attribute emotionClass numeric", "@attribute emotionClass {0,1,2,3,4,5,6,7,8}");
            StreamWriter wrt = new StreamWriter(Global.logsDir + "\\training.arff");
            wrt.Write(replacedData);
            wrt.Close();

            txtViewer.Text +="Convertion Finished" + System.Environment.NewLine;
            File.Delete(Global.logsDir + "\\training.csv");
        }
        public void trainingClassifier()
        {
            txtViewer.Text +=System.Environment.NewLine +System.Environment.NewLine +"Starting Training...." +System.Environment.NewLine;
            try
            {
                weka.core.Instances insts = new weka.core.Instances(new java.io.FileReader(Global.logsDir + "\\training.arff"));
                if (insts.classIndex() == -1)
                    insts.setClassIndex(insts.numAttributes() - 1);

                string typeClassifier = comboBoxClassifier.Text;
                weka.classifiers.Classifier cl = null;
                switch (typeClassifier)
                {
                    case "Random Forest":
                        cl = new weka.classifiers.trees.RandomForest();
                        // weka.classifiers.trees.RandomForest cl = new weka.classifiers.trees.RandomForest();
                        //cl.setNumTrees(10);
                        //cl.setNumFeatures(10);
                        break;
                    case "Naive Bayes":
                        cl = new weka.classifiers.bayes.NaiveBayes();
                        break;
                    case "Adaboost":
                        cl = new weka.classifiers.meta.AdaBoostM1();
                        break;
                }
                //CROSS-VALIDATION
                if (crossValidation.IsChecked==true)
                {
                    int nFolds = 0;
                    txtViewer.Text +="Starting Cross Validation with: " + txtFolds.Text.ToString() +"Folds"+ System.Environment.NewLine;
                    try{
                        nFolds= Convert.ToInt32(txtFolds.Text);
                        if (nFolds < 2)
                            {
                                MessageBox.Show("Number of folds has to be > 1!");
                            }
                        else
                            {
                                weka.classifiers.Evaluation eval = new weka.classifiers.Evaluation(insts);
                                Dispatcher.Invoke(new Action(()=>{
                                    eval.crossValidateModel(cl, insts, nFolds, new java.util.Random());
                                    txtViewer.Text += eval.toSummaryString();
                                }));
                            }
                    }
                    catch(FormatException f) {
                        MessageBox.Show("Please enter a valid number of folds!");
                    }
                    
                }
                cl.buildClassifier(insts);
                txtViewer.Text +="Model sucessfully trained." + System.Environment.NewLine;

                // serialize model
                txtViewer.Text +="Saving Model..." + System.Environment.NewLine;
                weka.core.SerializationHelper.write(Global.modelDir + "\\" + typeClassifier + ".model", cl);
                txtViewer.Text +="Model saved succesfully.";

            }
            catch (java.lang.Exception ex)
            {
                ex.printStackTrace();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                logsDir.Text = dialog.SelectedPath;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                modelDir.Text = dialog.SelectedPath;
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            //-- Setting global variables
            Global.logsDir = logsDir.Text;
            Global.modelDir = modelDir.Text;
            //Checking if training file exist
            bool checkTrainingArff = File.Exists(Global.logsDir + "\\training.arff");
            bool checkTrainingCsv = File.Exists(Global.logsDir + "\\training.csv");
            //If it exist go directly to the training 
            
            if (!checkTrainingArff)
            {
                string[] csvFiles = Directory.GetFiles(Global.logsDir, "*.csv");
                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(() =>
                {
                txtViewer.Text += "Number of LOG files found: " + csvFiles.Length + System.Environment.NewLine;  
                 }));
                MessageBox.Show("Gere");
                ////--Append Files 
                 if (!checkTrainingCsv)
                 {
                     appendFiles(csvFiles);
                 }
                 else
                 {
                     Dispatcher.Invoke(new Action(() =>
                     {
                         txtViewer.Text += "CSV Training file found" + System.Environment.NewLine;
                     }));
                 }
                ////--Convert CSV to ARFF
                csv2arff();
                Dispatcher.Invoke(new Action(() =>
                {
                    txtViewer.Text += "Training file generated" + System.Environment.NewLine;
                }));
                 
            }
            else{
                Dispatcher.Invoke(new Action(() =>
                {
                    txtViewer.Text += "ARFF Training file found, going directly to the training section" + System.Environment.NewLine;
                }));
            }
            txtViewer.InvalidateVisual();
            trainingClassifier();
        }

    }
}
