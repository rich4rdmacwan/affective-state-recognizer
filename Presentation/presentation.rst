:title: Empathic Robotic Tutor
:author: Daniel Sileshi, Richard Macwan, Roberto Marroquin
:description: A presentation for the Robotics Project module at Heriot Watt University
:keywords: presentation, Empathic Robotic Tutor, affective state recognition
:css: presentation.css
:skip-help: true
:data-transition-duration: 750
:data-perspective: 1000

--------

Outline
=======

- Problem statement

- Emotion recognition

- Methodology

- Implementation

- Results

- Future work

-----

Empathic Robotic Tutor
=======================

- Give a robot the capability to perform socially aware interaction *[1]*

- Detect if a child is happy or frustated and react accordingly

.. raw:: html

	 <img src="naotable.jpg" width=500 height=345/>


.. note::
	This learning scenario is a serious
	game called Enercities which is played on a large touch table

-----

Emotion recognition
===================
- Emotions reflect current human state or *Affective State*--Use pictures for emotions
- Not just limited to facial expression but body language, gestures and audio cues
- Focus on gestures and facial expressions

-----

Emotion recognition
===================
Facial Expressions
-------------------
- Facial Action Coding System: A research tool to determine facial expressions. 
- Expressions are broken down into Action Units (AUs) and Shape Units (SUs)

.. raw:: html
	
	<img src="facs1cameraAngles.png"/>

-----

Emotion recognition
===================
Facial Action Coding System
---------------------------
.. raw:: html
	
	<table style="position:relative;left:80px;font-size:18px">
	<tr>
	<td><img src="facs2TrackedPoints.png" height=250 /><br>Tracked Points</td>
	<td><img src="facs3HeadPoseAngles.png" height=250/><br>Head Pose Angles</td>
	</tr>
	</table>



-----

Emotion recognition
===================
Facial Expressions
-------------------
- The Kinect API *[2]* uses six AUs and 11 SUs which are a subset of what is defined in the Candide3 model

-----

Emotion recognition
===================
Facial Expressions AUs
----------------------

.. raw:: html
	
	<table style="position:relative;left:80px;font-size:18px">
	<tr>
	<td><img src="AU0UpperLipRaiser.png" width=300 height=200/><br>AU0: UpperLipRaiser</td>
	<td><img src="AU1JawLowerer.png" width=300 height=200/><br>AU1: JawLowerer</td>
	</tr>
	<tr>
	<td><img src="AU2LipStretcher.png" width=300 height=200/><br>AU2: LipStretcher</td>
	<td><img src="AU3BrowLowerer.png" width=300 height=200/><br>AU3: BrowLowerer</td>
	</tr>
	</table>

-----

Emotion recognition
===================
Facial Expressions AUs
----------------------

.. raw:: html
	
	<table style="position:relative;left:80px;font-size:18px">
	<tr>
	<td><img src="AU4LipCompresser.png" width=300 height=200/><br>AU4: LipCompressor</td>
	<td><img src="AU5OuterBrowRaiser.png" width=300 height=200/><br>AU5: OuterBrowRaiser</td>
	</tr>
	</table>

-----

Emotion recognition
===================
Gestures and Body Language
--------------------------
- Hand gestures, posture and other physical manifestations also determine affective state *[3]*

-----

Emotion recognition
===================
Gestures and Body Language
--------------------------
.. raw:: html
	
	<img src="gestureEmotions.png" height=500/>

-----

Emotion recognition
===================
.. raw:: html
	
	<img src="gestureEmotions2.png" height=500/> <i>[4]</i>

-----

Emotion recognition
===================
Kinect APIs
--------------------------
- Gestures
	- Kinect Body
- Facial Expressions
	- Kinect Face 
	- HighDefinition Face

-----

Emotion recognition
===================
Kinect APIs
--------------------------

- Gestures
	- Kinect Body : HandLeft, HandRight, HandTipLeft, HandTipRight, ElbowLeft, ElbowRight, HipLeft, HipRight, ShoulderLeft, ShoulderRight, SpineBase, SpineMid, SpineShoulder, ThumbLeft, ThumbRight, WristLeft, WristRight

-----

Emotion recognition
===================
Kinect APIs
--------------------------
- Facial Expressions
	- **Kinect Face** : x, y, z, MouthMoved, LeftEyeClosed, RightEyeClosed, LookingAway, Engaged
	- **HighDefinition Face** : JawOpen, JawSlideRight, LeftCheekPuff, LefteyeBrowLowerer, LefteyeClosed, LipCornerDepressorLeft, LipCornerDepressorRight, LipCornerPullerLeft, LipCornerPullerRight, LipPucker, LipStretcherLeft, LipStretcherRight, LowerlipDepressorLeft, LowerlipDepressorRight, RightcheekPuff, RighteyebrowLowerer,RighteyeClosed


-----

:data-rotate-z: -90
:data-scale: 5
:data-x: r6000


Methodology
===========

.. raw:: html

	 <img src="projectplan.png" width=700 height=400/>

------

Implementation
==============

- Data acquisition

- Labeling data

- Training classifier

- Real-time application

------

Data acquisition
================

.. raw:: html

	 <img src="daq.png" width=700 height=400/>


-----

Data acquisition
================


.. raw:: html

	 <img src="screenshot.png" height=500/>


----

Labeling data
=============

.. raw:: html

	 <img src="labelGUI.jpg" height=420/>



-----

Training classifier
===================

.. raw:: html

	 <img src="trainingGUI.jpg" height=420/>



-----

Machine Learning
================

.. raw:: html

	 <img src="figure1.png" height=550/>


--------

Random forest Training
======================

.. raw:: html

	 <img src="figure2.png" height=350/>


--------

Random Forest Testing
=====================

.. raw:: html

	 <img src="figure3.png" height=320/>


-----

Real-time Application
=====================

.. raw:: html

	 <img src="liveGUI.jpg" height=420/>



-----


:data-rotate-z: 0
:data-scale: 1
:data-y: r0
:data-x: r3000

Results
=======

- Confusion matrix

- Cross validation

- Variable of importance

- RF learning curve


-----

Confusion Matrix
================

.. raw:: html

	 <img src="figure4.png" height=220/>


--------

Cross validation 
================


.. raw:: html

	 <img src="figure5.png" height=300 style="position:relative; left:-40px	"/>

 10 Fold cross validation and 66:34 split accuracy


--------


Variable of Importance
======================

.. raw:: html

	 <img src="figure6a.png" width=900 style="position:relative; left:-50px"/>

--------


Variable of Importance
======================

.. raw:: html

	<img src="figure6b.png" width=900 style="position:relative; left:-50px"/>

--------

RF Learning curve
=================
.. raw:: html

	 <img src="figure7.png" height=550/>


--------


Future work
===========

- Train the classifier with more data, specially with kids

- Send the emotion output to a robot

- Tune properly the parameters of the classifiers

- Incorporate video feedback with improved performance 



----

:data-rotate-z: 90
:data-scale: 5
:data-y: r0
:data-x: r3000


References
==========
.. raw:: html
	
	<img src="thanksbot.png" height=200 style="position:relative; right:-550px;float:left" />
	<div style="text-align:left">
	<br>[1] <a href="http://www.emote-project.eu/">Emote Project</a>
	<br>[2] <a href="http://msdn.microsoft.com/en-us/library/jj130970.aspx">Kinect Face Tracking</a>
	<br>[3] <a href="http://www.cl.cam.ac.uk/research/rainbow/emotions/hand.html">Hand Over Face Gestures</a>
	<br>[4] <a href="http://fc09.deviantart.net/fs70/f/2011/111/a/b/gesture___emotions_1_by_chuunin7-d3eixme.jpg">DeviantArt</a>
	
	</div>