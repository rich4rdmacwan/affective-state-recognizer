﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using Microsoft.Kinect.Face;
using Microsoft.Kinect.Tools;
using System.Windows.Controls.Primitives;
using System.Collections;
using System.Windows.Threading;
using Microsoft.Win32;
using System.ComponentModel;
using System.Windows.Media.Animation;
using System.Globalization;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading;

namespace LiveAffectiveStateRecognizer
    {


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
        {

        private KinectSensor kinectSensor = null;
        /// <summary>
        /// String to store feature data
        /// </summary>
        /// 
        public static String alldata;
        private string kin1_1 = null;
        private string kin2_1 = null;
        private string kin3_1 = null;
        private string kin1_2 = null;
        private string kin2_2 = null;
        private string kin3_2 = null;
        //Strings to be used for classification
        private string cl1 = null;
        private string cl2 = null;
        private string cl3 = null;

        /// <summary>
        /// Description (width, height, etc) of the infrared frame data
        /// </summary>
        private FrameDescription infraredFrameDescription = null;
        private FrameDescription colorFrameDescription = null;




        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private System.Drawing.Graphics bitmapGraphics;

        private WriteableBitmap colorBitmap = null;

        private WriteableBitmap infraredBitmap = null;
        /// <summary>
        /// Maximum value (as a float) that can be returned by the InfraredFrame
        /// </summary>
        private const float InfraredSourceValueMaximum = (float)ushort.MaxValue;


        /// <summary>
        /// The value by which the infrared source data will be scaled
        /// </summary>
        private const float InfraredSourceScale = 1.0f;

        /// <summary>
        /// Smallest value to display when the infrared data is normalized
        /// </summary>
        private const float InfraredOutputValueMinimum = 0.001f;

        /// <summary>
        /// Largest value to display when the infrared data is normalized
        /// </summary>
        private const float InfraredOutputValueMaximum = (float)ushort.MaxValue;

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private const int BytesPerPixel = 4;

        private enum DisplaySource { Color, Infrared/*, Depth, BodyIndex */};

        private DisplaySource KinectDisplaySource
            {
            get;
            set;
            }



        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;


        /// <summary>
        /// Current tracking id of person
        /// </summary>
        private ulong currentTrackingId = 0;
        private ulong CurrentTrackingId
            {
            get
                {
                return this.currentTrackingId;

                }

            set
                {
                this.currentTrackingId = value;
                }
            }

        private bool BodySelected = false;
        public bool KinectEnabled
            {
            get { return (bool)GetValue(KinectEnabledProperty); }
            set { SetValue(KinectEnabledProperty, value); }
            }
        public static readonly DependencyProperty KinectEnabledProperty =
            DependencyProperty.Register("KinectEnabled", typeof(bool), typeof(MainWindow), new UIPropertyMetadata(false));
        
        public bool FaceDetected
            {
            get { return (bool)GetValue(FaceDetectedProperty); }
            set { SetValue(FaceDetectedProperty, value); }
            }
        public static readonly DependencyProperty FaceDetectedProperty =
            DependencyProperty.Register("FaceDetected", typeof(bool), typeof(MainWindow), new UIPropertyMetadata(false));


        //public static readonly DependencyProperty BodySelectedProperty =
        //     DependencyProperty.Register("BodySelected", typeof(bool),
        //       typeof(MainWindow), new UIPropertyMetadata(false));
        private HighDefinitionFaceFrameSource highDefinitionFaceFrameSource;
        private HighDefinitionFaceFrameReader highDefinitionFaceFrameReader;


        /// <summary>
        /// Timer for classification
        /// </summary>
        private System.Timers.Timer classificationTimer;
        double[] classificationResult ;//= { 0.06, 0.1, 0.07, 0.7, 0.03, 0.01, 0.01, 0.02 };
        double[] classificationResult1 ;//= { 0.1, 0.06, 0.07, 0.01, 0.03, 0.7, 0.02, 0.01 };
        weka.classifiers.Classifier classifier;
        DoubleAnimation[][] animation;
        Storyboard[] storyboard;
        List<Image> emoticonList;
        double maxEmoticonZoom = 2;
        bool alt = false;
        //variables for classification
        java.io.BufferedReader arff_filereader;
        weka.core.converters.ArffLoader.ArffReader arffreader;

        // Create an empty training set
        weka.core.Instances TestBucket;

        Hashtable[] nominalAttIndices = new Hashtable[5];
        weka.core.DenseInstance iExample1;
        private readonly BackgroundWorker worker = new BackgroundWorker();
        Action _cancelWork;
        private string[] emotionStrings = { "Happy", "Angry", "Sad", "Frustrated", "Confused", "Surprised", "Afraid", "Disgusted" };

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;
        /// <summary>
        /// Array to store bodies
        /// </summary>
        private Body[] bodies = null;

        private int bodyCount;//We will only track one body
        private MultiSourceFrameReader multisourceFrameReader = null;
        private bool isColor = true;
        /// <summary>
        /// Face frame sources
        /// </summary>
        private FaceFrameSource[] faceFrameSources = null;

        /// <summary>
        /// Face frame readers
        /// </summary>
        private FaceFrameReader[] faceFrameReaders = null;

        /// <summary>
        /// Storage for face frame results
        /// </summary>
        private FaceFrameResult[] faceFrameResults = null;

        /// <summary>
        /// Width of display (color space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (color space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// Display rectangle
        /// </summary>
        private Rect displayRect;

        /// <summary>
        /// List of brushes for each face tracked
        /// </summary>
        private List<Brush> faceBrush;
        bool updateRect = false;
        private byte[] colorBitmapPixels;
        private int colorBitmapStride;
        private int colorBitmapBytesPerPixel;
        private int faceRectX, faceRectY, faceRectXEnd, faceRectYEnd;

        // specify the required face frame results
        FaceFrameFeatures faceFrameFeatures =
            FaceFrameFeatures.BoundingBoxInColorSpace
            | FaceFrameFeatures.PointsInColorSpace
            | FaceFrameFeatures.RotationOrientation
            | FaceFrameFeatures.FaceEngagement
            | FaceFrameFeatures.Glasses
            | FaceFrameFeatures.Happy
            | FaceFrameFeatures.LeftEyeClosed
            | FaceFrameFeatures.RightEyeClosed
            | FaceFrameFeatures.LookingAway
            | FaceFrameFeatures.MouthMoved
            | FaceFrameFeatures.MouthOpen;

        private FaceAlignment currentFaceAlignment = null;
        /// <summary>
        /// FaceModel is a result of capturing a face
        /// </summary>
        private FaceModel currentFaceModel = null;

        /// <summary>
        /// FaceModelBuilder is used to produce a FaceModel
        /// </summary>
        private FaceModelBuilder faceModelBuilder = null;
        private int emotionIndex=-1;
        private bool classifierReady=false;
        private int progress = 0;
        
        public MainWindow()
            {
            // get the kinectSensor object
            this.kinectSensor = KinectSensor.GetDefault();
            this.bodyCount = this.kinectSensor.BodyFrameSource.BodyCount;
            // allocate storage to store body objects
            this.bodies = new Body[this.bodyCount];
            
            // set IsAvailableChanged event notifier
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // set the display specifics
            FrameDescription frameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
            this.displayWidth = frameDescription.Width;
            this.displayHeight = frameDescription.Height;
            this.displayRect = new Rect(0.0, 0.0, this.displayWidth, this.displayHeight);



            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // use the window object as the view model in this simple example
            this.DataContext = this;
            
            KinectEnabled = true;
            InitKinectReaders();
            
            pixelformat = new PixelFormat();
            pixelformat = PixelFormats.Bgra32;
            


            classificationTimer = new System.Timers.Timer(1000);
            classificationTimer.Elapsed += classificationTimer_Elapsed;

            // use the window object as the view model in this simple example
            this.DataContext = this;

            this.InitializeComponent();



            //Animating the emoticons
            animation = new DoubleAnimation[8][];

            storyboard = new Storyboard[8];
            emoticonList = new List<Image>();
            emoticonList.Add(imgHappy); emoticonList.Add(imgAngry); emoticonList.Add(imgSad);
            emoticonList.Add(imgFrustrated); emoticonList.Add(imgConfused); emoticonList.Add(imgSurprised);
            emoticonList.Add(imgScared); emoticonList.Add(imgDisgusted);

            //animationList[i].AutoReverse = true;
            for (int i = 0; i < storyboard.Length; i++)
                {
                animation[i] = new DoubleAnimation[3];
                animation[i][0] = new DoubleAnimation(); //For translate (to make the icon rise)
                animation[i][1] = new DoubleAnimation(); //For scaleX (to zoom the prominent emotion icon) 
                animation[i][2] = new DoubleAnimation(); //For scaleX (to zoom the prominent emotion icon) 

                //Translate transform
                animation[i][0].Duration = TimeSpan.FromMilliseconds(300);
                animation[i][0].BeginTime = TimeSpan.Parse("0:0:0");
                animation[i][0].From = 10;
                animation[i][0].To = -90;

                //Scale Transform
                animation[i][1].Duration = TimeSpan.FromMilliseconds(300);
                animation[i][1].BeginTime = TimeSpan.Parse("0:0:0");
                animation[i][1].From = 1;
                animation[i][1].To = 1;

                animation[i][2].Duration = TimeSpan.FromMilliseconds(300);
                animation[i][2].BeginTime = TimeSpan.Parse("0:0:0");
                animation[i][2].From = 1;
                animation[i][2].To = 1;

                storyboard[i] = new Storyboard();
                //storyboard[i].RepeatBehavior = RepeatBehavior.Forever;
                storyboard[i].Children.Add(animation[i][0]);
                storyboard[i].Children.Add(animation[i][1]);
                storyboard[i].Children.Add(animation[i][2]);

                Storyboard.SetTargetProperty(animation[i][0],
                    new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(TranslateTransform.Y)"));
                Storyboard.SetTarget(animation[i][0], emoticonList[i]);

                Storyboard.SetTargetProperty(animation[i][1],
                    new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[1].(ScaleTransform.ScaleY)"));
                Storyboard.SetTarget(animation[i][1], emoticonList[i]);

                Storyboard.SetTargetProperty(animation[i][2],
                    new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[1].(ScaleTransform.ScaleX)"));

                Storyboard.SetTarget(animation[i][2], emoticonList[i]);
                emoticonList[i].RenderTransformOrigin = new Point(0.5, 0.5);
                }


            for (int i = 0; i < nominalAttIndices.Length; i++)
                {
                nominalAttIndices[i] = new Hashtable();
                }        
                

            //Initialize classifier stuff

            //MouthMoved and LeftEyeClosed values
            for (int i = 0; i < 2; i++)
                {
                nominalAttIndices[i].Add("Unknown", 0);
                nominalAttIndices[i].Add("Yes", 1);
                nominalAttIndices[i].Add("No", 2);
                nominalAttIndices[i].Add("Maybe", 3);
                }
            //RightEyeClosed
            nominalAttIndices[2].Add("Unknown", 0);
            nominalAttIndices[2].Add("Maybe", 1);
            nominalAttIndices[2].Add("No", 2);
            nominalAttIndices[2].Add("Yes", 3);

            //LookingAway
            nominalAttIndices[3].Add("Yes", 0);
            nominalAttIndices[3].Add("Maybe", 1);
            nominalAttIndices[3].Add("No", 2);

            //Engaged
            nominalAttIndices[4].Add("No", 0);
            nominalAttIndices[4].Add("Maybe", 1);
            nominalAttIndices[4].Add("Yes", 2);

            try
                {
                var cancellationTokenSource = new CancellationTokenSource();

                this._cancelWork = () =>
                {
                   // this.StopButton.IsEnabled = false;
                    cancellationTokenSource.Cancel();
                };

                var limit = 10;

                var progressReport = new Progress<int>((i) => {
                this.txtStatistics.Text ="Loading classifier "+ (100 * i / (limit - 1)).ToString() + "%";
                progressBar.Value = 100 * i / (limit - 1);
                        }
                    );

                var token = cancellationTokenSource.Token;

                 Task.Run(() =>
                    DoWork(limit, token, progressReport),
                    token);
                }
            catch (Exception ex)
                {
                MessageBox.Show(ex.Message);
                }
            
            //classificationTimer.Start();
            }

        private  void DoWork(int limit,CancellationToken token,IProgress<int> progressReport)
            {
            //Start the classification timer to display progress of the loading classifier
            classificationTimer.Start();
            

            TimeSpan begin = Process.GetCurrentProcess().TotalProcessorTime;
            
            arff_filereader = new java.io.BufferedReader(new java.io.FileReader("../../Face_traindata.arff"));
            progressReport.Report(progress++);
            arffreader = new weka.core.converters.ArffLoader.ArffReader(arff_filereader);
            progressReport.Report(progress ++);
            java.io.ObjectInputStream ois = new java.io.ObjectInputStream(new java.io.FileInputStream("../../RF-Face.model"));
            progressReport.Report(progress++);
            classifier = (weka.classifiers.Classifier)ois.readObject();
            progressReport.Report(progress+= 5);
            ois.close();
            progressReport.Report(progress++);
            
            TestBucket = new weka.core.Instances(arffreader.getStructure(), 1);
            
            TestBucket.setClassIndex(TestBucket.numAttributes() - 1);
            iExample1 = new weka.core.DenseInstance(TestBucket.numAttributes() - 1);
            
            TimeSpan end = Process.GetCurrentProcess().TotalProcessorTime;
            Console.WriteLine("Time to read java object: " + (end - begin).TotalMilliseconds + " ms");
            progressReport.Report(progress++);
        //    token.ThrowIfCancellationRequested();
            
          //  return limit;

            //Flag to indicate that the classifier is ready
            classifierReady = true;
            //Restart the timer to start classification in real time
            classificationTimer.Stop();
            classificationTimer.Start();

            }

        private void worker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
            {
            
            }

        private void worker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
            {
            classificationTimer.Start();
            }

        private void InitializeFaceStuff()
            {
            InitializeHDFace();
            // create a face frame source + reader to track each face in the FOV
            this.faceFrameSources = new FaceFrameSource[this.bodyCount];
            this.faceFrameReaders = new FaceFrameReader[this.bodyCount];
            for (int i = 0; i < this.bodyCount; i++)
                {
                // create the face frame source with the required face frame features and an initial tracking Id of 0
                this.faceFrameSources[i] = new FaceFrameSource(this.kinectSensor, 0, faceFrameFeatures);

                // open the corresponding reader
                this.faceFrameReaders[i] = this.faceFrameSources[i].OpenReader();
                }

            // allocate storage to store face frame results for each face in the FOV
            this.faceFrameResults = new FaceFrameResult[this.bodyCount];

            // populate face result colors - one for each face index
            this.faceBrush = new List<Brush>()
            {
                Brushes.White, 
                Brushes.Orange,
                Brushes.Green,
                Brushes.Red,
                Brushes.LightBlue,
                Brushes.Yellow
            };
            for (int i = 0; i < this.bodyCount; i++)
                {
                if (this.faceFrameReaders[i] != null)
                    {
                    // wire handler for face frame arrival
                    this.faceFrameReaders[i].FrameArrived += this.Reader_FaceFrameArrived;
                    }
                }
            
            this.kinectSensor.Open();
            
            }

        void classificationTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
            {
            if (classifierReady)
                {
                updateRect = true;
                //Construct classification string and classify
                //Check if cl1, cl2 and cl3 have the right number of features. only then perform classification
                //if (cl1 != null && cl2 != null && cl3 != null)
                if (cl1 != null && cl3 != null)
                    {
                    TimeSpan begin = Process.GetCurrentProcess().TotalProcessorTime;

                    //string cl = cl1 + "," + cl2 + "," + cl3;
                    string cl = cl1 + "," + cl3;
                    //if ((cl1.Split(',')).Length == 8 && (cl2.Split(',')).Length == 17 && (cl3.Split(',')).Length == 17)
                    if ((cl1.Split(',')).Length == 8 && (cl3.Split(',')).Length == 17)
                        {

                        string[] data = cl.Split(',');

                        for (int i = 0; i < data.Length; i++)
                            {
                            double x;
                            bool check = double.TryParse(data[i], out x);
                            //    Console.WriteLine("the currrent token: " + data[i]);
                            if (check == true)
                                {
                                iExample1.setValue(i, double.Parse(data[i]));
                                }

                            else
                                {
                                string key = data[i].ToString().Trim();
                                Hashtable ht = nominalAttIndices[i - 3];
                                int index = (int)ht[key];
                                iExample1.setValue(i, index);
                                }
                            }
                        TestBucket.add(iExample1);
                        //Console.WriteLine(TestBucket.numInstances() + ":" + DateTime.Now.ToLongTimeString() + DateTime.Now.Millisecond);
                        weka.core.Instance inst = TestBucket.lastInstance();
                        emotionIndex = (int)classifier.classifyInstance(inst);
                        classificationResult = classifier.distributionForInstance(TestBucket.lastInstance());


                        //Get classification result
                        //classificationResult= classifier.classify(c1);
                        //Check with the guys how to do this correctly.

                        //Send the result for displaying the emotion
                        Dispatcher.Invoke(new Action(() =>
                        {
                            lblHappy.Content = classificationResult[0].ToString();
                            lblAngry.Content = classificationResult[1].ToString();
                            lblSad.Content = classificationResult[2].ToString();
                            lblFrustrated.Content = classificationResult[3].ToString();
                            lblConfused.Content = classificationResult[4].ToString();
                            lblSurprised.Content = classificationResult[5].ToString();
                            lblAfraid.Content = classificationResult[6].ToString();
                            lblDisgusted.Content = classificationResult[7].ToString();
                            txtStatistics.Text = emotionStrings[emotionIndex];
                            //Find max of the array and set that emoticon
                            //classificationResult.Max

                            TestBucket.clear();
                            showEmotion();
                        }));

                        TimeSpan end = Process.GetCurrentProcess().TotalProcessorTime;
                        Console.WriteLine("Time to classify: " + (end - begin).TotalMilliseconds + " ms - emotionIndex = " + emotionIndex);
                        cl1 = null;
                        cl2 = null;
                        cl3 = null;
                        }
                    }
                }
            else
                {

                }
            }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
            {
            

            }

        private void Reader_FaceFrameArrived(object sender, FaceFrameArrivedEventArgs e)
            {
            using (FaceFrame faceFrame = e.FrameReference.AcquireFrame())
                {
                if (faceFrame != null)
                    {
                    // get the index of the face source from the face source array
                    int index = this.GetFaceSourceIndex(faceFrame.FaceFrameSource);

                    // check if this face frame has valid face frame results
                    if (this.ValidateFaceBoxAndPoints(faceFrame.FaceFrameResult))
                        {
                        // store this face frame result to later draw
                        this.faceFrameResults[index] = faceFrame.FaceFrameResult;
                        var result = this.faceFrameResults[index];
                        var rotation = result.FaceRotationQuaternion;
                        int x, y, z;
                        ExtractFaceRotationInDegrees(rotation, out x, out y, out z);
                        cl1 = x.ToString() + "," + y.ToString() + "," + z.ToString() + "," + //result.FaceProperties[FaceProperty.Happy].ToString() + "," +
                            result.FaceProperties[FaceProperty.MouthMoved].ToString() +
                        "," + result.FaceProperties[FaceProperty.LeftEyeClosed].ToString() + "," + result.FaceProperties[FaceProperty.RightEyeClosed].ToString() +
                        /*"," + result.FaceProperties[FaceProperty.WearingGlasses].ToString() + */ "," + result.FaceProperties[FaceProperty.LookingAway].ToString() +
                        "," + result.FaceProperties[FaceProperty.Engaged].ToString();
                        //Console.WriteLine(cl1);

                        }
                    else
                        {
                        // indicates that the latest face frame result from this reader is invalid
                        this.faceFrameResults[index] = null;
                        }
                    }
                }
            }

        private void ExtractFaceRotationInDegrees(Vector4 rotQuaternion, out int pitch, out int yaw, out int roll)
            {
            double x = rotQuaternion.X;
            double y = rotQuaternion.Y;
            double z = rotQuaternion.Z;
            double w = rotQuaternion.W;

            double yawD, pitchD, rollD;
            pitchD = Math.Atan2(2 * ((y * z) + (w * x)), (w * w) - (x * x) - (y * y) + (z * z)) / Math.PI * 180.0;
            yawD = Math.Asin(2 * ((w * y) - (x * z))) / Math.PI * 180.0;
            rollD = Math.Atan2(2 * ((x * y) + (w * z)), (w * w) + (x * x) - (y * y) - (z * z)) / Math.PI * 180.0;

            double increment = 1;
            pitch = (int)((pitchD + ((increment / 2.0) * (pitchD > 0 ? 1.0 : -1.0))) / increment) * (int)increment;
            yaw = (int)((yawD + ((increment / 2.0) * (yawD > 0 ? 1.0 : -1.0))) / increment) * (int)increment;
            roll = (int)((rollD + ((increment / 2.0) * (rollD > 0 ? 1.0 : -1.0))) / increment) * (int)increment;
            }

        /// <summary>
        /// Validates face bounding box and face points to be within screen space
        /// </summary>
        /// <param name="faceResult">the face frame result containing face box and points</param>
        /// <returns>success or failure</returns>
        private bool ValidateFaceBoxAndPoints(FaceFrameResult faceResult)
            {
            bool isFaceValid = faceResult != null;

            if (isFaceValid)
                {
                var faceBox = faceResult.FaceBoundingBoxInColorSpace;
                if (faceBox != null)
                    {
                    // check if we have a valid rectangle within the bounds of the screen space
                    isFaceValid = (faceBox.Right - faceBox.Left) > 0 &&
                                  (faceBox.Bottom - faceBox.Top) > 0 &&
                                  faceBox.Right <= this.displayWidth &&
                                  faceBox.Bottom <= this.displayHeight;

                    if (isFaceValid)
                        {
                        var facePoints = faceResult.FacePointsInColorSpace;
                        if (facePoints != null)
                            {
                            foreach (PointF pointF in facePoints.Values)
                                {
                                // check if we have a valid face point within the bounds of the screen space
                                bool isFacePointValid = pointF.X > 0.0f &&
                                                        pointF.Y > 0.0f &&
                                                        pointF.X < this.displayWidth &&
                                                        pointF.Y < this.displayHeight;

                                if (!isFacePointValid)
                                    {
                                    isFaceValid = false;
                                    break;
                                    }
                                }
                            }
                        }
                    }
                }

            return isFaceValid;
            }

        private void InitKinectReaders()
            {

            this.colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
            this.colorBitmap = new WriteableBitmap(this.colorFrameDescription.Width, this.colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgra32, null);
            this.colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;
            this.colorBitmapBytesPerPixel = colorBitmap.Format.BitsPerPixel / 8;
            this.colorBitmapStride = colorBitmap.PixelWidth * (colorBitmap.Format.BitsPerPixel) / 8;
            this.colorBitmapPixels = new byte[colorBitmapStride * colorBitmap.PixelHeight];
            this.faceRectX = 0;
            this.faceRectY = 0;
            this.faceRectXEnd = 100;
            this.faceRectYEnd = 100;

            this.infraredFrameDescription = this.kinectSensor.InfraredFrameSource.FrameDescription;
            this.infraredBitmap = new WriteableBitmap(this.infraredFrameDescription.Width, this.infraredFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray32Float, null);
            System.Drawing.Bitmap b = new System.Drawing.Bitmap(this.infraredFrameDescription.Width, this.infraredFrameDescription.Height, this.infraredFrameDescription.Width * 4, System.Drawing.Imaging.PixelFormat.Format32bppPArgb, infraredBitmap.BackBuffer);
            bitmapGraphics = System.Drawing.Graphics.FromImage(b);

            this.multisourceFrameReader = this.kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Body | FrameSourceTypes.BodyIndex | FrameSourceTypes.Infrared | FrameSourceTypes.Depth );
            this.multisourceFrameReader.MultiSourceFrameArrived += multisourceFrameReader_MultiSourceFrameArrived;


            }

        private void multisourceFrameReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
            {
            MultiSourceFrame mframe = e.FrameReference.AcquireFrame();
            if (isColor)
                {
                using (var colorFrame = mframe.ColorFrameReference.AcquireFrame())
                    {
                    if (colorFrame != null)
                        {
                        FrameDescription colorFrameDescription = colorFrame.FrameDescription;

                        using (KinectBuffer colorBuffer = colorFrame.LockRawImageBuffer())
                            {
                            this.colorBitmap.Lock();

                            // verify data and write the new color frame data to the display bitmap
                            if ((colorFrameDescription.Width == this.colorBitmap.PixelWidth) && (colorFrameDescription.Height == this.colorBitmap.PixelHeight))
                                {

                                colorFrame.CopyConvertedFrameDataToIntPtr(
                                    this.colorBitmap.BackBuffer,
                                    (uint)(colorFrameDescription.Width * colorFrameDescription.Height * 4),
                                    ColorImageFormat.Bgra);
                                //DrawRectangle(colorBitmapPixels, faceRectX, faceRectY, faceRectXEnd, faceRectYEnd, 2);

                                this.colorBitmap.AddDirtyRect(new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight));
                                }

                            this.colorBitmap.Unlock();
                            //works
                            //this.colorBitmap.DrawRectangle((int)faceRect.X, (int)faceRect.Y, (int)faceRect.Width, (int)faceRect.Height, Colors.Red);
                            //this.colorBitmap.DrawRectangle((int)faceRect.X, 347, 400, 300, Colors.Red);
                            }
                        }

                    }

                using (var bodyFrame = mframe.BodyFrameReference.AcquireFrame())
                    {
                    if (bodyFrame != null)
                        {
                        // update body data
                        bodyFrame.GetAndRefreshBodyData(this.bodies);
                        // iterate through each face source
                        for (int i = 0; i < this.bodyCount; i++)
                            {
                            // check if a valid face is tracked in this face source
                            if (this.faceFrameSources[i].IsTrackingIdValid)
                                {
                                
                                // check if we have valid face frame results
                                if (this.faceFrameResults[i] != null)
                                    {

                                    // draw face frame results
                                    //if (updateRect)
                                    //    {
                                    //    this.DrawFaceFrameResults(i, this.faceFrameResults[i]);
                                    //    updateRect = false;
                                    //    }
                                    FaceDetected = true;
                                    
                                    }
                                else
                                    FaceDetected = false;
                                    
                                }
                            
                            //else
                              //  {
                                // check if the corresponding body is tracked 
                                if (this.bodies[i].IsTracked)
                                    {
                                    // update the face frame source to track this body
                                    this.faceFrameSources[i].TrackingId = this.bodies[i].TrackingId;
                                    
                                    this.highDefinitionFaceFrameSource.TrackingId = this.bodies[i].TrackingId;
                                    //TODO: Set hdface tracking id

                                    var target = bodies[i];
                                    cl2 = target.Joints[JointType.HandLeft].Position.X.ToString() + "," + target.Joints[JointType.HandRight].Position.X.ToString() + "," +
                                    target.Joints[JointType.HandTipLeft].Position.X.ToString() + "," + target.Joints[JointType.HandTipRight].Position.X.ToString()
                                     + "," + target.Joints[JointType.ElbowLeft].Position.X.ToString() + "," + target.Joints[JointType.ElbowRight].Position.Y.ToString()
                                     + "," + target.Joints[JointType.HipLeft].Position.X.ToString() + "," + target.Joints[JointType.HipRight].Position.Y.ToString()
                                     + "," + target.Joints[JointType.ShoulderLeft].Position.X.ToString() + "," + target.Joints[JointType.ShoulderRight].Position.Y.ToString()
                                     + "," + target.Joints[JointType.SpineBase].Position.X.ToString() + "," + target.Joints[JointType.SpineMid].Position.Y.ToString()
                                     + "," + target.Joints[JointType.SpineShoulder].Position.X.ToString()
                                     + "," + target.Joints[JointType.ThumbLeft].Position.X.ToString() + "," + target.Joints[JointType.ThumbRight].Position.Y.ToString()
                                     + "," + target.Joints[JointType.WristLeft].Position.X.ToString() + "," + target.Joints[JointType.WristRight].Position.Y.ToString();
                                    //Console.WriteLine(cl2);
                                    
                                    }
                                //}
                            }
                        }
                    }
                }
            else
                {
                using (InfraredFrame infraredFrame = mframe.InfraredFrameReference.AcquireFrame())
                    {
                    if (infraredFrame != null)
                        {
                        // the fastest way to process the infrared frame data is to directly access 
                        // the underlying buffer
                        using (Microsoft.Kinect.KinectBuffer infraredBuffer = infraredFrame.LockImageBuffer())
                            {
                            // verify data and write the new infrared frame data to the display bitmap
                            if (((this.infraredFrameDescription.Width * this.infraredFrameDescription.Height) == (infraredBuffer.Size / this.infraredFrameDescription.BytesPerPixel)) &&
                                (this.infraredFrameDescription.Width == this.infraredBitmap.PixelWidth) && (this.infraredFrameDescription.Height == this.infraredBitmap.PixelHeight))
                                {
                                this.ProcessInfraredFrameData(infraredBuffer.UnderlyingBuffer, infraredBuffer.Size);

                                }
                            }

                        }
                    }

                }
            }

        private void DrawFaceFrameResults(int i, FaceFrameResult faceResult)
            {
            // draw the face bounding box
            var faceBoxSource = faceResult.FaceBoundingBoxInColorSpace;
            //Rect faceBox = new Rect(faceBoxSource.Left, faceBoxSource.Top, faceBoxSource.Right - faceBoxSource.Left, faceBoxSource.Bottom - faceBoxSource.Top);
            faceRectX = faceBoxSource.Left;
            faceRectY = faceBoxSource.Top;
            faceRectXEnd = faceBoxSource.Right;
            faceRectYEnd = faceBoxSource.Bottom;

            }

        void DrawRectangle(byte[] sourcePixels, int x, int y, int right, int bottom, int linewidth)
            {
            unsafe
                {
                byte* pBackBuffer = (byte*)colorBitmap.BackBuffer;
                byte pixel = (byte)(0x05);
                for (int row = y; row < bottom; row++)
                    {
                    int rowIndex = colorBitmapStride * row;
                    for (int col = x; col < right; col++)
                        {
                        int index = rowIndex + colorBitmapBytesPerPixel * col;
                        //pixels[index] = (byte)(0x0f);
                        *(pBackBuffer + index + 2) = pixel;
                        //*(pBackBuffer + index + 2) = pixel;

                        }
                    }
                ////Top horiz line
                //for (int row = y; row < y + linewidth; row++)
                //    {
                //    for (int col = x; col < right; col++)
                //        {
                //        int index = colorBitmapStride * row + colorBitmapBytesPerPixel * col;
                //        //pixels[index] =(byte)(0x0f);
                //        *(pBackBuffer + index + 1) = pixel;
                //        //*(pBackBuffer + index + 2) = (byte)(0x0f);

                //        }
                //    }
                ////Bottom horiz line
                //for (int row = bottom - linewidth; row < bottom; row++)
                //    {
                //    for (int col = x; col < right; col ++)
                //        {
                //        int index = colorBitmapStride * row + colorBitmapBytesPerPixel * col;
                //        //pixels[index] =(byte)(0x0f);
                //        *(pBackBuffer + index + 1) = pixel;
                //        //*(pBackBuffer + index + 2) = (byte)(0x0f);

                //        }
                //    }
                //Left vert line
                //for (int row = y + linewidth; row < bottom - linewidth; row++)
                //    {
                //    for (int col = x; col < x + linewidth; col++)
                //        {
                //        int index = colorBitmapStride * row + colorBitmapBytesPerPixel * col;
                //        //pixels[index] =(byte)(0x0f);
                //        *(pBackBuffer + index + 1) = pixel;
                //        //*(pBackBuffer + index + 2) = (byte)(0x0f);

                //        }
                //    }
                ////Right vert line
                //for (int row = y + linewidth; row < bottom - linewidth; row++)
                //    {
                //    for (int col = right - linewidth; col < right; col++)
                //        {
                //        int index = colorBitmapStride * row + colorBitmapBytesPerPixel * col;
                //        //pixels[index] =(byte)(0x0f);
                //        *(pBackBuffer + index + 1) = pixel;
                //        //*(pBackBuffer + index + 2) = (byte)(0x0f);
                //        }
                //    }
                }

            }

        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
            {
            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
            if (this.kinectSensor.IsOpen)
                {
                statusImage.Source = new BitmapImage(new Uri(@"/Images/StatusOn.png", UriKind.Relative));
                }
            else
                {
                statusImage.Source = new BitmapImage(new Uri(@"/Images/Status.png", UriKind.Relative));
                }
            }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
            {
            get
                {
                return this.statusText;
                }

            set
                {
                if (this.statusText != value)
                    {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                        {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                        }
                    }
                }
            }

        /// <summary>
        /// Returns the index of the face frame source
        /// </summary>
        /// <param name="faceFrameSource">the face frame source</param>
        /// <returns>the index of the face source in the face source array</returns>
        private int GetFaceSourceIndex(FaceFrameSource faceFrameSource)
            {
            int index = -1;

            for (int i = 0; i < this.bodyCount; i++)
                {
                if (this.faceFrameSources[i] == faceFrameSource)
                    {
                    index = i;
                    break;
                    }
                }

            return index;
            }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
            {
            for (int i = 0; i < this.bodyCount; i++)
                {
                if (this.faceFrameReaders[i] != null)
                    {
                    // FaceFrameReader is IDisposable
                    this.faceFrameReaders[i].Dispose();
                    this.faceFrameReaders[i] = null;
                    }

                if (this.faceFrameSources[i] != null)
                    {
                    // FaceFrameSource is IDisposable
                    this.faceFrameSources[i].Dispose();
                    this.faceFrameSources[i] = null;
                    }
                }

            if (this.multisourceFrameReader != null)
                {
                // DepthFrameReader is IDisposable
                this.multisourceFrameReader.Dispose();
                this.multisourceFrameReader = null;
                }


            if (this.kinectSensor != null)
                {
                this.kinectSensor.Close();
                this.kinectSensor = null;
                }


            }



        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource imageToDisplay
            {
            get
                {
                if (isColor)
                    return this.colorBitmap;
                else
                    return this.infraredBitmap;
                }

            }



        private void CheckBox_Checked(object sender, RoutedEventArgs e)
            {
            if (KinectEnabled)
                {
                if (highDefinitionFaceFrameSource == null)
                    InitializeFaceStuff();
                    
                }
            else
                {
                kinectSensor.Open();
                }
            }

        private void InitializeHDFace()
            {
            this.kinectSensor = KinectSensor.GetDefault();
            //this.currentFaceModel = new FaceModel();
            this.currentFaceAlignment = new FaceAlignment();

            
            this.multisourceFrameReader = kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Body | FrameSourceTypes.Infrared | FrameSourceTypes.BodyIndex | FrameSourceTypes.Depth);
            this.multisourceFrameReader.MultiSourceFrameArrived += multisourceFrameReader_MultiSourceFrameArrived; //event for multiple source (Position)

            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            this.highDefinitionFaceFrameSource = new HighDefinitionFaceFrameSource(kinectSensor);
            this.highDefinitionFaceFrameSource.TrackingIdLost += this.HdFaceSource_TrackingIdLost;

            this.highDefinitionFaceFrameReader = this.highDefinitionFaceFrameSource.OpenReader();
            this.highDefinitionFaceFrameReader.FrameArrived += this.HdFaceReader_FrameArrived; //event gor high def face
            
            

            }

        private void HdFaceReader_FrameArrived(object sender, HighDefinitionFaceFrameArrivedEventArgs e)
            {
            if (!KinectEnabled) return;
            using (var frame = e.FrameReference.AcquireFrame())
                {
                // We might miss the chance to acquire the frame; it will be null if it's missed.
                // Also ignore this frame if face tracking failed.
                if (frame == null || !frame.IsFaceTracked)
                    {
                    //Console.WriteLine("Frame is null");
                    return;
                    }
                

                frame.GetAndRefreshFaceAlignmentResult(this.currentFaceAlignment);

                var JawOpen = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.JawOpen].ToString("0.000");
                var JawSlideRight = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.JawSlideRight].ToString("0.000");
                var LeftcheekPuff = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LeftcheekPuff].ToString("0.000");
                var LefteyebrowLowerer = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LefteyebrowLowerer].ToString("0.000");
                var LefteyeClosed = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LefteyeClosed].ToString("0.000");
                var LipCornerDepressorLeft = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LipCornerDepressorLeft].ToString("0.000");
                var LipCornerDepressorRight = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LipCornerDepressorRight].ToString("0.000");
                var LipCornerPullerLeft = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LipCornerPullerLeft].ToString("0.000");
                var LipCornerPullerRight = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LipCornerPullerRight].ToString("0.000");
                var LipPucker = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LipPucker].ToString("0.000");
                var LipStretcherLeft = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LipStretcherLeft].ToString("0.000");
                var LipStretcherRight = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LipStretcherRight].ToString("0.000");
                var LowerlipDepressorLeft = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LowerlipDepressorLeft].ToString("0.000");
                var LowerlipDepressorRight = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.LowerlipDepressorRight].ToString("0.000");
                var RightcheekPuff = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.RightcheekPuff].ToString("0.000");
                var RighteyebrowLowerer = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.RighteyebrowLowerer].ToString("0.000");
                var RighteyeClosed = currentFaceAlignment.AnimationUnits[FaceShapeAnimations.RighteyeClosed].ToString("0.000");

                
                //JawOpen,JawSlideRight,LeftcheekPuff,LefteyebrowLowerer,LefteyeClosed,LipCornerDepressorLeft,LipCornerDepressorRight,LipCornerPullerLeft,LipCornerPullerRight,LipPucker,LipStretcherLeft,LipStretcherRight,LowerlipDepressorLeft,LowerlipDepressorRight,RightcheekPuff,RighteyebrowLowerer,RighteyeClosed
                cl3 = JawOpen.ToString() + "," + JawSlideRight.ToString() + "," + LeftcheekPuff.ToString() +
                    "," + LefteyebrowLowerer.ToString() + "," + LefteyeClosed.ToString() + "," +
                    LipCornerDepressorLeft.ToString() + "," + LipCornerDepressorRight.ToString() + "," +
                    LipCornerPullerLeft.ToString() + "," + LipCornerPullerRight.ToString() + "," + LipPucker.ToString() +
                    "," + LipStretcherLeft.ToString() + "," + LipStretcherRight.ToString() + "," +
                    LowerlipDepressorLeft.ToString() + "," + LowerlipDepressorRight.ToString() + "," +
                    RightcheekPuff.ToString() + "," + RighteyebrowLowerer.ToString() + "," + RighteyeClosed.ToString();
                
                }
            }

        private void HdFaceSource_TrackingIdLost(object sender, TrackingIdLostEventArgs e)
            {
            var lostTrackingID = e.TrackingId;

            if (this.CurrentTrackingId == lostTrackingID)
                {
                this.CurrentTrackingId = 0;
                
                this.highDefinitionFaceFrameSource.TrackingId = 0;
                
                }
            }


        public PixelFormat pixelformat { get; set; }

        private void chkBody_Checked(object sender, RoutedEventArgs e)
            {
            BodySelected = ((CheckBox)sender).IsChecked.Value;
            }
        private unsafe void ProcessInfraredFrameData(IntPtr infraredFrameData, uint infraredFrameDataSize)
            {
            // infrared frame data is a 16 bit value
            ushort* frameData = (ushort*)infraredFrameData;

            // lock the target bitmap
            this.infraredBitmap.Lock();

            // get the pointer to the bitmap's back buffer
            float* backBuffer = (float*)this.infraredBitmap.BackBuffer;

            // process the infrared data
            for (int i = 0; i < (int)(infraredFrameDataSize / this.infraredFrameDescription.BytesPerPixel); ++i)
                {
                // since we are displaying the image as a normalized grey scale image, we need to convert from
                // the ushort data (as provided by the InfraredFrame) to a value from [InfraredOutputValueMinimum, InfraredOutputValueMaximum]
                backBuffer[i] = Math.Min(InfraredOutputValueMaximum, (((float)frameData[i] / InfraredSourceValueMaximum * InfraredSourceScale) * (1.0f - InfraredOutputValueMinimum)) + InfraredOutputValueMinimum);
                }

            bitmapGraphics.Flush(System.Drawing.Drawing2D.FlushIntention.Flush);
            // mark the entire bitmap as needing to be drawn
            this.infraredBitmap.AddDirtyRect(new Int32Rect(0, 0, this.infraredBitmap.PixelWidth, this.infraredBitmap.PixelHeight));
            // unlock the bitmap

            this.infraredBitmap.Unlock();

            }



        private void SourceSelectionChanged(object sender, RoutedEventArgs e)
            {
            RadioButton source = ((RadioButton)sender);
            switch (source.Name)
                {
                case "rdoColor":
                    isColor = true;
                    break;
                case "rdoInfrared":
                    isColor = false;

                    break;
                //case "Depth":
                //    KinectDisplaySource = DisplaySource.Depth;
                //    break;
                //case "BodyIndex":
                //    KinectDisplaySource = DisplaySource.BodyIndex;
                //    break;
                }
            if (this.PropertyChanged != null)
                {
                this.PropertyChanged(this, new PropertyChangedEventArgs("imageToDisplay"));
                }

            }
        private void showEmotion(object sender, MouseEventArgs m)
            {

            //int i=emoticonList.FindIndex(x => x.Name == ((Image)sender).Name);
            alt = true;
            if (alt)
                {
                //double maxValue = classificationResult.Max();
                int maxIndex = (int)emotionIndex;
                if (((Image)sender).Name == "imgSurprised")
                    maxIndex = 5;
                Console.WriteLine("Emotion : " + maxIndex);
                for (int i = 0; i < 8; i++)
                    {
                    storyboard[i].Stop();

                    if (i == maxIndex)
                        {
                        animation[i][0].From = 10;
                        animation[i][0].To = -60;
                        animation[i][1].From = 1;
                        animation[i][1].To = maxEmoticonZoom;
                        animation[i][2].From = 1;
                        animation[i][2].To = maxEmoticonZoom;
                        emoticonList[i].Opacity = 1;
                        }
                    else
                        {
                        animation[i][0].From = animation[i][0].To;
                        animation[i][0].To = 10;
                        animation[i][1].From = animation[i][1].To;
                        animation[i][1].To = 1;

                        animation[i][1].From = animation[i][1].To;
                        animation[i][2].To = 1;
                        emoticonList[i].Opacity = 0.7;
                        }
                    storyboard[i].Begin();

                    }

                }
            }

        private void showEmotion()
            {

            //int i=emoticonList.FindIndex(x => x.Name == ((Image)sender).Name);
            alt = true;
            if (alt)
                {
                //double maxValue = classificationResult.Max();
                int maxIndex = (int)emotionIndex;
             //   if (((Image)sender).Name == "imgSurprised")
               //     maxIndex = 5;
                //Console.WriteLine("Emotion : " + maxIndex);
                for (int i = 0; i < 8; i++)
                    {
                    storyboard[i].Stop();

                    if (i == maxIndex)
                        {
                        animation[i][0].From = animation[i][0].To;
                        animation[i][0].To = -60;
                        animation[i][1].From = animation[i][1].To;
                        animation[i][1].To = maxEmoticonZoom;
                        animation[i][2].From = animation[i][2].To;
                        animation[i][2].To = maxEmoticonZoom;
                        emoticonList[i].Opacity = 1;
                        }
                    else
                        {
                        animation[i][0].From = animation[i][0].To;
                        animation[i][0].To = 10;
                        
                        animation[i][1].From = animation[i][1].To;
                        animation[i][1].To = 1;

                        animation[i][1].From = animation[i][1].To;
                        animation[i][2].To = 1;
                        emoticonList[i].Opacity = 0.7;
                        }
                    storyboard[i].Begin();

                    }

                }

            }

        }

    }
