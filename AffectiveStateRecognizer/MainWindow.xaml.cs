﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.AffectiveStateRecognizer
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using Microsoft.Kinect.Face;
    using Microsoft.Kinect.Tools;
    using System.Windows.Controls.Primitives;
    using System.Collections;
    using System.Text;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Windows.Threading;
    using System.Windows.Input;
    using Microsoft.Win32;

    /// <summary>
    /// Interaction logic for the MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        /// <summary>
        /// String to store feature data
        /// </summary>
        /// 
        public static String alldata;

        private string kin1_1 = null;
        //Kinect feature names
        //MouthOpen missed in perception logs
        private string[] kin1_1Features = { "x:", "y:", "z:", "Happy:", "MouthMoved:", "LeftEyeClosed:", "RightEyeClosed:", "WearingGlasses:", "LookingAway:", "Engaged:" };
        private int[] kin1_1FeaturesToIgnore = { 3, 7 };
        private string[] kin2_1Features = { "HandLeft:", "HandRight:", "HandTipLeft:", "HandTipRight:", "ElbowLeft:", "ElbowRight:", "HipLeft:", "HipRight:", "ShoulderLeft:", "ShoulderRight:", "SpineBase:", "SpineMid:", "SpineShoulder:", "ThumbLeft:", "ThumbRight:", "WristLeft:", "WristRight:" };
        private string[] kin3_1Features = { "JawOpen:", "JawSlideRight:", "LeftcheekPuff:", "LefteyebrowLowerer:", "LefteyeClosed:", "LipCornerDepressorLeft:", "LipCornerDepressorRight:", "LipCornerPullerLeft:", "LipCornerPullerRight:", "LipPucker:", "LipStretcherLeft:", "LipStretcherRight:", "LowerlipDepressorLeft:", "LowerlipDepressorRight:", "RightcheekPuff:", "RighteyebrowLowerer:", "RighteyeClosed:" };

        //For ease of referring to each feature group
        private string[][] kinFeatures = new string[3][];
        private string[] featureNames;
        private string logHeader;
        private bool mlLogHeaderWritten = false;

        private string kin2_1 = null;
        private string kin3_1 = null;
        private string kin1_2 = null;
        private string kin2_2 = null;
        private string kin3_2 = null;

        private Body[] bodies;
        //Flags to mark if frames are still arriving
        bool frameArrived = false;
        /// <summary>
        /// Maximum value (as a float) that can be returned by the InfraredFrame
        /// </summary>
        private const float InfraredSourceValueMaximum = (float)ushort.MaxValue;

        /// <summary>
        /// The value by which the infrared source data will be scaled
        /// </summary>
        private const float InfraredSourceScale = 1.0f;

        /// <summary>
        /// Smallest value to display when the infrared data is normalized
        /// </summary>
        private const float InfraredOutputValueMinimum = 0.001f;

        /// <summary>
        /// Largest value to display when the infrared data is normalized
        /// </summary>
        private const float InfraredOutputValueMaximum = 1.0f;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Reader for infrared frames
        /// </summary>
        private InfraredFrameReader infraredFrameReader = null;

        private MultiSourceFrameReader multisourceFrameReader = null;


        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Reader for face frames
        /// </summary>
        private FaceFrameReader faceReader = null;
        /// <summary>
        /// Source for face frames
        /// </summary>

        private FaceFrameSource faceSource = null;

        /// <summary>
        /// Source for hdface frames
        /// </summary>
        private HighDefinitionFaceFrameSource hdfaceFrameSource = null;


        /// <summary>
        /// Reader for hdface frames
        /// </summary>
        private HighDefinitionFaceFrameReader hdfaceFrameReader = null;

        /// <summary>
        /// Description (width, height, etc) of the infrared frame data
        /// </summary>
        private FrameDescription infraredFrameDescription = null;


        /// <summary>
        /// Bitmap to display
        /// </summary>
        private WriteableBitmap infraredBitmap = null,croppedBitmap=null;
        

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        /// <summary>
        /// Flag for detected person
        /// </summary>
        Boolean DetectedPerson;

        /// <summary>
        /// Current tracking id of person
        /// </summary>
        private ulong currentTrackingId = 0;
        private ulong CurrentTrackingId
        {
            get
            {
                return this.currentTrackingId;
            }

            set
            {
                this.currentTrackingId = value;
            }
        }

        /// <summary>
        /// Face Features to track. We only select the features that we need
        /// </summary>
        private const FaceFrameFeatures DefaultFaceFrameFeatures = FaceFrameFeatures.PointsInColorSpace
                                | FaceFrameFeatures.FaceEngagement
                                | FaceFrameFeatures.LeftEyeClosed
                                | FaceFrameFeatures.RightEyeClosed
                                | FaceFrameFeatures.MouthOpen
                                | FaceFrameFeatures.MouthMoved
                                | FaceFrameFeatures.LookingAway
                                | FaceFrameFeatures.RotationOrientation;

        private FaceAlignment currentFaceAlignment = null;
        //Path to perception logs TODO: change this to correct path
        string logDirectory = @"E:\Studies\ViBOT\Sem3\RoboticsProject\Affective State Recognizer\PerceptionLogs\logs";
        //Path to destination log directory
        string tagLogDirectory = @"E:\Studies\ViBOT\Sem3\RoboticsProject\Affective State Recognizer\TagLogs";

        //The creation time of the .xef file        
        DateTime creationTime;
        //The time when an emotion starts
        DateTime emotionStartTime, emotionEndTime, frameTime, temp;
        string line, mlLine, prevMlLine,prevLine;

        StringReader stringReader;
        StringReader mlstringReader;


        //To keep track of the time between emotion start and end
        Stopwatch frameTimer;

        //For cropping the infrared frame
        TransformGroup renderTransform = new TransformGroup();
        ScaleTransform scaleTransform = new ScaleTransform();
        System.Drawing.Graphics bitmapGraphics;
        System.Drawing.Pen pen;
        Rectangle cropRect;
        private bool mouseInCroppedRect = false;
        private bool mouseInCroppedRectAndDown = false;
        private double anchorX, anchorY;
        private double prevX, prevY;

        private enum Mode{Live,Tagging,Training};
        private Mode ActiveMode;
        RectangleGeometry transformedImageControlRectGeometry;
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            
            // get the kinectSensor object
            this.kinectSensor = KinectSensor.GetDefault();

            frameTimer = new Stopwatch();
            //this.multisourceFrameReader = this.kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Body | FrameSourceTypes.BodyIndex | FrameSourceTypes.Infrared | FrameSourceTypes.Depth);
            //this.multisourceFrameReader.MultiSourceFrameArrived += multisourceFrameReader_MultiSourceFrameArrived;


          
         
            //More tagging information
            kinFeatures[0] = kin1_1Features;
            kinFeatures[1] = kin2_1Features;
            kinFeatures[2] = kin3_1Features;

            //Build log header string
            int i = 0;
            foreach (string feature in kin1_1Features)
                {
                if (i != kin1_1FeaturesToIgnore[0] && i != kin1_1FeaturesToIgnore[1])
                    logHeader += feature.Split(':')[0] + ",";
                ++i;
                }
            foreach (string feature in kin2_1Features)
                {
                logHeader += feature.Split(':')[0] + ",";

                }
            foreach (string feature in kin3_1Features)
                {
                logHeader += feature.Split(':')[0] + ",";
                }
            logHeader = logHeader + "emotionClass";

            setupMediaPlayer();

                  

            
            this.currentFaceAlignment = new FaceAlignment();

            
            // set IsAvailableChanged event notifier
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;


            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // use the window object as the view model in this simple example
            this.DataContext = this;




            InitTaggingStuff();    
            this.InitializeComponent();
            
          
        }

        private void InitTaggingStuff()
            {
            this.infraredFrameReader = this.kinectSensor.InfraredFrameSource.OpenReader();


            // wire handler for frame arrival
            this.infraredFrameReader.FrameArrived += this.Reader_InfraredFrameArrived;

            // get FrameDescription from InfraredFrameSource
            this.infraredFrameDescription = this.kinectSensor.InfraredFrameSource.FrameDescription;

            // create the bitmap to display
            this.infraredBitmap = new WriteableBitmap(this.infraredFrameDescription.Width, this.infraredFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray32Float, null);
            this.croppedBitmap = this.infraredBitmap;
            System.Drawing.Bitmap b = new System.Drawing.Bitmap(this.infraredFrameDescription.Width, this.infraredFrameDescription.Height, this.infraredFrameDescription.Width * 4, System.Drawing.Imaging.PixelFormat.Format32bppPArgb, infraredBitmap.BackBuffer);

            bitmapGraphics = System.Drawing.Graphics.FromImage(b);
            bitmapGraphics.SmoothingMode = SmoothingMode.HighQuality;
            bitmapGraphics.InterpolationMode = InterpolationMode.NearestNeighbor;
            bitmapGraphics.CompositingMode = CompositingMode.SourceOver;
            bitmapGraphics.CompositingQuality = CompositingQuality.HighQuality;
            pen = new System.Drawing.Pen(System.Drawing.SystemColors.ButtonHighlight, 1);
            DrawingContext dc = (new DrawingVisual()).RenderOpen();
            dc.DrawImage(infraredBitmap, new Rect(0, 0, this.infraredFrameDescription.Width, this.infraredFrameDescription.Height));
            dc.Close();
            cropRect = new Rectangle(200, 150, 120, 90);
            transformedImageControlRectGeometry = new RectangleGeometry(new Rect(cropRect.X, cropRect.Y, cropRect.Width, cropRect.Height));
             
            }


        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private string tagLogFileName, mlLogFileName;

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.infraredBitmap;
            }
        }

        public ImageSource CroppedImageSource
        {
            get
            {
                return this.croppedBitmap;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.infraredFrameReader != null)
            {
                // InfraredFrameReader is IDisposable
                this.infraredFrameReader.Dispose();
                this.infraredFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        /// <summary>
        /// Handles the user clicking on the screenshot button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void ScreenshotButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.infraredBitmap != null)
            {
                // create a png bitmap encoder which knows how to save a .png file
                BitmapEncoder encoder = new PngBitmapEncoder();

                // create frame from the writable bitmap and add to encoder
                encoder.Frames.Add(BitmapFrame.Create(this.infraredBitmap));

                string time = System.DateTime.Now.ToString("hh'-'mm'-'ss", CultureInfo.CurrentUICulture.DateTimeFormat);

                string myPhotos = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

                string path = Path.Combine(myPhotos, "KinectScreenshot-Infrared-" + time + ".png");

                // write the new file to disk
                try
                {
                    // FileStream is IDisposable
                    using (FileStream fs = new FileStream(path, FileMode.Create))
                    {
                        encoder.Save(fs);
                    }

                    this.StatusText = string.Format(CultureInfo.CurrentCulture, Properties.Resources.SavedScreenshotStatusTextFormat, path);
                }
                catch (IOException)
                {
                    this.StatusText = string.Format(CultureInfo.CurrentCulture, Properties.Resources.FailedScreenshotStatusTextFormat, path);
                }
            }
        }

        /// <summary>
        /// Handles the infrared frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_InfraredFrameArrived(object sender, InfraredFrameArrivedEventArgs e)
        {
            if (!frameArrived)
            {
                frameArrived = true;
                scaleTransform.ScaleX = 3.5;
                scaleTransform.ScaleY = 3.5;
                scaleTransform.CenterX = infraredFrameDescription.Width / 2;
                scaleTransform.CenterY = infraredFrameDescription.Height / 2;
                renderTransform.Children.Add(scaleTransform);
                transformedImageControl.RenderTransform = renderTransform;

                frameTimer.Start();
            }
            // InfraredFrame is IDisposable
            using (InfraredFrame infraredFrame = e.FrameReference.AcquireFrame())
            {
                if (infraredFrame != null)
                {
                    // the fastest way to process the infrared frame data is to directly access 
                    // the underlying buffer
                    using (Microsoft.Kinect.KinectBuffer infraredBuffer = infraredFrame.LockImageBuffer())
                    {
                        // verify data and write the new infrared frame data to the display bitmap
                        if (((this.infraredFrameDescription.Width * this.infraredFrameDescription.Height) == (infraredBuffer.Size / this.infraredFrameDescription.BytesPerPixel)) &&
                            (this.infraredFrameDescription.Width == this.infraredBitmap.PixelWidth) && (this.infraredFrameDescription.Height == this.infraredBitmap.PixelHeight))
                        {
                            this.ProcessInfraredFrameData(infraredBuffer.UnderlyingBuffer, infraredBuffer.Size);
                        }
                    }

                    Dispatcher.Invoke(new Action(() =>
                      {

                          frameTime = creationTime.Add(frameTimer.Elapsed);
                          lblMessage.Content = frameTime.ToString();
                      }));



                }
            }
        }

        /// <summary>
        /// Directly accesses the underlying image buffer of the InfraredFrame to 
        /// create a displayable bitmap.
        /// This function requires the /unsafe compiler option as we make use of direct
        /// access to the native memory pointed to by the infraredFrameData pointer.
        /// </summary>
        /// <param name="infraredFrameData">Pointer to the InfraredFrame image data</param>
        /// <param name="infraredFrameDataSize">Size of the InfraredFrame image data</param>
        private unsafe void ProcessInfraredFrameData(IntPtr infraredFrameData, uint infraredFrameDataSize)
        {
            // infrared frame data is a 16 bit value
            ushort* frameData = (ushort*)infraredFrameData;

            // lock the target bitmap
            this.infraredBitmap.Lock();

            // get the pointer to the bitmap's back buffer
            float* backBuffer = (float*)this.infraredBitmap.BackBuffer;

            // process the infrared data
            for (int i = 0; i < (int)(infraredFrameDataSize / this.infraredFrameDescription.BytesPerPixel); ++i)
            {
                // since we are displaying the image as a normalized grey scale image, we need to convert from
                // the ushort data (as provided by the InfraredFrame) to a value from [InfraredOutputValueMinimum, InfraredOutputValueMaximum]
                backBuffer[i] = Math.Min(InfraredOutputValueMaximum, (((float)frameData[i] / InfraredSourceValueMaximum * InfraredSourceScale) * (1.0f - InfraredOutputValueMinimum)) + InfraredOutputValueMinimum);
            }

            

            bitmapGraphics.DrawRectangle(pen, cropRect);

            bitmapGraphics.Flush(System.Drawing.Drawing2D.FlushIntention.Flush);
            // mark the entire bitmap as needing to be drawn
            this.infraredBitmap.AddDirtyRect(new Int32Rect(0, 0, this.infraredBitmap.PixelWidth, this.infraredBitmap.PixelHeight));
            // unlock the bitmap
            
            
            this.infraredBitmap.Unlock();
            this.croppedBitmap = this.infraredBitmap;
            
        

        }

        
        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;

        }

        private void EmoButton_Clicked(object sender, RoutedEventArgs e)
        {
            IEnumerator en = ButtonList.Children.GetEnumerator();
            ToggleButton current;
            int index = 0, classIndex = 0;
            var emotion=((ToggleButton)sender).Content.ToString();
            System.Console.WriteLine("Saving data for emotion : " + emotion);
            while (en.MoveNext())
            {
                ++index;
                current = ((ToggleButton)en.Current);
                if (current.Name != ((ToggleButton)sender).Content.ToString())
                {
                    current.IsEnabled = !current.IsEnabled;
                }
                else
                {
                    classIndex = index;
                }

            }

            if (((ToggleButton)sender).IsChecked == true)
            {
                emotionStartTime = frameTime;

            }
            else
            {
                emotionEndTime = frameTime;
                
                //Emotion range determined. Now we should write the log data within this timeframe from the perception logs to a new file

                string logDataToWrite = "";
                string mlLogDataToWrite = "";
            
                //Timestamp of first line from the log file
                DateTime emoStartFromLog = creationTime;
                int result = DateTime.Compare(emoStartFromLog, emotionStartTime);

                //result identifies whether the timestamp from the log is equal to or later than the timestamp from the kinect file
                //To write into the new log, emoStartFromLog must be later or equal to emotionStartTime
                //First find the correct position
                while (result < 0)
                {
                    line = stringReader.ReadLine();
                    //Parse to write into the mlLog
                    mlLine = mlstringReader.ReadLine();

                    //Extract required features for the ml log. All this data will have the class undetermined(0)
                    mlLine = extractMlLogString(mlLine);
                    //TODO HACK: Might receive unchanging data from perception logs
                    if (mlLine != prevMlLine)
                        mlLogDataToWrite += (mlLine + "0" + System.Environment.NewLine);


                    if (line == null)
                        temp = DateTime.Parse(prevLine.Split(',')[0]);
                    else
                        temp = DateTime.Parse(line.Split(',')[0]);
                    emoStartFromLog = new DateTime(creationTime.Year, creationTime.Month, creationTime.Day, temp.Hour, temp.Minute, temp.Second, temp.Millisecond);
                    result = DateTime.Compare(emoStartFromLog, emotionStartTime);
                    if (line == null) break;
                    prevMlLine = mlLine;
                    prevLine = line;
                    
                }
                if (line == null) line = prevLine;
                temp = DateTime.Parse(line.Split(',')[0]);
                DateTime emoEndFromLog = new DateTime(creationTime.Year, creationTime.Month, creationTime.Day, temp.Hour, temp.Minute, temp.Second, temp.Millisecond);
                result = DateTime.Compare(emoEndFromLog, emotionEndTime);
                //Extract comma seperated tokens from each line
                string[] tokens = line.Split(',');

                while (result < 0)
                {
                    string newline = extractKinectLogString(tokens);

                    //Parse to write into the mlLog
                    mlLine = mlstringReader.ReadLine();
                    if (mlLine == null) break;
                    //Add data  to mlLog. The class here corresponds to the selected emotion
                    mlLine = extractMlLogString(mlLine);
                    
                    //TODO HACK: Might receive unchanging data from perception logs
                    if (mlLine!=null )
                        mlLogDataToWrite += (mlLine + classIndex + System.Environment.NewLine);


                    //Also discard the unneccessary action unit values to save to the log for ML
                    //Append log data to be written
                    logDataToWrite +=(newline + emotion + System.Environment.NewLine);
                    //TODO: Also parse the line and extract the required features


                    line = stringReader.ReadLine();
                    if (line == null)
                        break;
                    tokens = line.Split(',');
                    temp = DateTime.Parse(tokens[0]);
                    emoEndFromLog = new DateTime(creationTime.Year, creationTime.Month, creationTime.Day, temp.Hour, temp.Minute, temp.Second, temp.Millisecond);
                    result = DateTime.Compare(emoEndFromLog, emotionEndTime);
                    prevMlLine = mlLine;
                }
                System.Console.WriteLine(emoStartFromLog.ToString()+ " to "+emoEndFromLog.ToString());
                //System.Console.WriteLine(logDataToWrite);
                StreamWriter writer = new StreamWriter(tagLogFileName, true);
                //writer.WriteLine(((ToggleButton)sender).Content);
                writer.Write(logDataToWrite);
                writer.Close();

                writer = new StreamWriter(mlLogFileName, true);
                if (!mlLogHeaderWritten)
                {
                    writer.WriteLine(logHeader);
                    mlLogHeaderWritten = true;
                }
                writer.Write(mlLogDataToWrite);
                writer.Close();
              
            }

        }

        private string extractMlLogString(string mlLine)
        {
          
            string[] mltokens = mlLine.Split(',');
            string newline = "";
            int kin12or3 = 3;//0 means kin1, 1 means kin2 and 2 means kin3
            int tokenIndex = -1;

            foreach (string token in mltokens)
            {
                //If the token starts with kin1_1,kin2_1 or kin3_1, 
                //attach the corresponding action unit names and then add them to then newline structure 
                //Need to use this version of StartsWith for ignoring case because of "Kin2_1" instead of "kin2_1"
                if (token.StartsWith("kin", true, CultureInfo.CurrentCulture))
                {
                    tokenIndex = 0;
                    if (token.StartsWith("kin1"))
                    {
                        kin12or3 = 0; //Just to be double sure
                        newline += token.Split(':')[1] + ",";
                    }
                    else if (token.StartsWith("Kin2"))
                    {
                        kin12or3 = 1;
                        newline += token.Split(':')[1] + ",";
                    }
                    else if (token.StartsWith("kin3"))
                    {
                        kin12or3 = 2;
                        newline += token.Split(':')[1] + ",";
                    }
                }
                else if (kin12or3 < 3) //Are we extracting the tokens of a kin1_1, kin2_1 or kin3_1 part?
                {
                    ++tokenIndex;
                    //Add only required features
                    if (kin12or3 == 0)
                    {
                        if (tokenIndex == kin1_1FeaturesToIgnore[0] || tokenIndex == kin1_1FeaturesToIgnore[1])
                            continue;
                        else
                            newline += token + ",";
                    }
                    else
                    {
                        newline += token + ",";
                    }

                }
                //otherwise DO NOT add it

            }

            return newline;
        }

        private string extractKinectLogString(string[] tokens)
        {
            string newline = "";
            int kin12or3 = 3;//0 means kin1, 1 means kin2 and 2 means kin3
            int tokenIndex = -1;
            foreach (string token in tokens)
            {
                //If the token starts with kin1_1,kin2_1 or kin3_1, 
                //attach the corresponding action unit names and then add them to then newline structure 
                //Need to use this version of StartsWith for ignoring case because of "Kin2_1" instead of "kin2_1"
                if (token.StartsWith("kin", true, CultureInfo.CurrentCulture))
                {
                    tokenIndex = 0;
                    if (token.StartsWith("kin1"))
                    {
                        kin12or3 = 0; //Just to be double sure
                        newline += "kin1_1-" + kin1_1Features[tokenIndex] + token.Split(':')[1] + ",";
                    }
                    else if (token.StartsWith("Kin2"))
                    {
                        kin12or3 = 1;
                        newline += "kin2_1-" + kin2_1Features[tokenIndex] + token.Split(':')[1] + ",";
                    }
                    else if (token.StartsWith("kin3"))
                    {
                        kin12or3 = 2;
                        newline += "kin3_1-" + kin3_1Features[tokenIndex] + token.Split(':')[1] + ",";
                    }
                }
                else if (kin12or3 < 3) //Are we extracting the tokens of a kin1_1, kin2_1 or kin3_1 part?
                {
                    ++tokenIndex;

                    newline += kinFeatures[kin12or3][tokenIndex] + token + ",";

                }
                else //otherwise just add it as is
                {
                    newline += token + ",";

                }
            }
            return newline;
        }



        private void chkKinectSourceActive_Checked(object sender, RoutedEventArgs e)
        {
            //this.hdfaceFrameSource = new HighDefinitionFaceFrameSource(this.kinectSensor);
            //this.hdfaceFrameSource.TrackingIdLost += hdfaceFrameSource_TrackingIdLost;

            //this.hdfaceFrameReader = this.hdfaceFrameSource.OpenReader();
            //this.hdfaceFrameReader.FrameArrived += hdfaceFrameReader_FrameArrived;
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            Stream checkStream = null;
            openFileDialog.Multiselect = false;
            openFileDialog.InitialDirectory = "F:\\Repository";
            //if you want filter only .txt file
            //dlg.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            //if you want filter all files            
            openFileDialog.Filter = "All Image Files | *.xef";

            openFileDialog.Multiselect = false;
            if ((bool)openFileDialog.ShowDialog())
            {
                try
                {
                    if ((checkStream = openFileDialog.OpenFile()) != null)
                    {
                        txtFile.Text = openFileDialog.FileName;
                        FileInfo fileInfo = new FileInfo(openFileDialog.FileName);
                        txtLogDirectory.Text = logDirectory + "\\log" + fileInfo.Name.Substring(0, fileInfo.Name.Length - 3) + "txt";
                        creationTime = fileInfo.CreationTime;
                        //Read all the log data into string
                        var filestream = new FileStream(txtLogDirectory.Text,
                            FileMode.Open,
                            FileAccess.Read);
                        var file = new StreamReader(filestream, Encoding.UTF8, true, 128);
                        alldata = file.ReadToEnd();
                        //Create a string reader for the data of the log file so that we can navigate through it and place the emotion tags at the right place
                        stringReader = new StringReader(alldata);
                        mlstringReader = new StringReader(alldata);

                        line = stringReader.ReadLine();
                        temp = DateTime.Parse(line.Split(',')[0]);
                        creationTime = new DateTime(creationTime.Year, creationTime.Month, creationTime.Day, temp.Hour, temp.Minute, temp.Second, temp.Millisecond);
                        //Filename for new log
                        tagLogFileName = tagLogDirectory + "\\" + fileInfo.Name.Substring(0, fileInfo.Name.Length - 3) + "csv"; ;
                        mlLogFileName = tagLogDirectory + "\\ml" + fileInfo.Name.Substring(0, fileInfo.Name.Length - 3) + "csv"; ;
                        file.Close();
                        filestream.Close();

                        //Erase tagLogFile if present
                        FileInfo tagFile = new FileInfo(tagLogFileName);
                        if (tagFile.Exists)
                        {
                            tagFile.Delete();
                        }

                        FileInfo mlFile = new FileInfo(mlLogFileName);
                        if (mlFile.Exists)
                        {
                            mlFile.Delete();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
            IEnumerator en = ButtonList.Children.GetEnumerator();
            while (en.MoveNext())
            {
                ToggleButton current = ((ToggleButton)en.Current);
                current.IsEnabled = true;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            Stream checkStream = null;
            openFileDialog.Multiselect = false;
            if ((bool)openFileDialog.ShowDialog())
            {
                try
                {
                    if ((checkStream = openFileDialog.OpenFile()) != null)
                    {
                        txtLogDirectory.Text = openFileDialog.FileName;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }

        }

        private void Image_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            //Check whether mouse moves inside the cropRect
            //If yes, change cursor
            System.Windows.Point pos=e.GetPosition((IInputElement)sender);

            if (mouseInCroppedRectAndDown)
            {
                //Move the rectangle
                cropRect.X = (int)(pos.X - anchorX);
                cropRect.Y = (int)(pos.Y - anchorY);
                transformedImageControlRectGeometry.Rect = new Rect(cropRect.X, cropRect.Y, cropRect.Width, cropRect.Height);
                transformedImageControl.Clip = transformedImageControlRectGeometry;
                
                
            }
            else
            {
                if (pos.X < cropRect.X + cropRect.Width && pos.X > cropRect.X
                    && pos.Y < cropRect.Y + cropRect.Height && pos.Y > cropRect.Y
                    )
                {
                    this.Cursor = ((System.Windows.Controls.TextBlock)this.Resources["CursorGrab"]).Cursor;
                    mouseInCroppedRect = true;
                }
                else
                {
                    this.Cursor = System.Windows.Input.Cursors.Arrow;
                    mouseInCroppedRect = false;
                }
            }
        }

        private void Image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (mouseInCroppedRect)
            {
                System.Windows.Point pos = e.GetPosition((IInputElement)sender);
                this.Cursor = ((System.Windows.Controls.TextBlock)this.Resources["CursorGrabbed"]).Cursor;
                mouseInCroppedRectAndDown = true;
                anchorX = pos.X-cropRect.X;
                anchorY = pos.Y-cropRect.Y;
                prevX = pos.X;
                prevY = pos.Y;
            }
            else
            {
                mouseInCroppedRectAndDown = false;
            }
        }

        private void Image_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            mouseInCroppedRectAndDown = false;
        }

        private void Image_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (mouseInCroppedRect)
            {
                //Increase or decrease the size of the rectangle
                if (e.Delta > 0)
                {

                    cropRect.Width -= 6;
                    cropRect.Height -= 6;
                    cropRect.X += 3;
                    cropRect.Y += 3;

                    scaleTransform.ScaleX += 0.2;
                    scaleTransform.ScaleY += 0.2;

                }
                else if (e.Delta < 0)
                {
                    cropRect.Width += 6;
                    cropRect.Height += 6;
                    cropRect.X -= 3;
                    cropRect.Y -= 3;

                    
                    scaleTransform.ScaleX -= 0.2;
                    scaleTransform.ScaleY -= 0.2;
                   
                }
                transformedImageControl.Clip = new RectangleGeometry(new Rect(cropRect.X, cropRect.Y, cropRect.Width, cropRect.Height));
                
            }
        }

        //Media player stuff

         private bool mediaPlayerIsPlaying = false;
                private bool userIsDraggingSlider = false;

                public void setupMediaPlayer()
                {

                        DispatcherTimer timer = new DispatcherTimer();
                        timer.Interval = TimeSpan.FromSeconds(1);
                        timer.Tick += timer_Tick;
                        timer.Start();
                }

                private void timer_Tick(object sender, EventArgs e)
                {
                        if((mePlayer.Source != null) && (mePlayer.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
                        {
                                sliProgress.Minimum = 0;
                                sliProgress.Maximum = mePlayer.NaturalDuration.TimeSpan.TotalSeconds;
                                sliProgress.Value = mePlayer.Position.TotalSeconds;
                        }
                }

                private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
                {
                        e.CanExecute = true;
                }

                private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
                {
                        OpenFileDialog openFileDialog = new OpenFileDialog();
                        openFileDialog.InitialDirectory=@"E:\Studies\ViBOT\Sem3\RoboticsProject\Data Expt\Videos";
                        openFileDialog.Filter = "Media files (All files (*.*)|*.*|*.mp4;*.MP4;*.mp3;*.mpg;*.mpeg)|*.mp3;*.mpg;*.mpeg";
                        if(openFileDialog.ShowDialog() == true)
                                mePlayer.Source = new Uri(openFileDialog.FileName);
                }

                private void Play_CanExecute(object sender, CanExecuteRoutedEventArgs e)
                {
                        e.CanExecute = (mePlayer != null) && (mePlayer.Source != null);
                }

                private void Play_Executed(object sender, ExecutedRoutedEventArgs e)
                {
                        mePlayer.Play();
                        mediaPlayerIsPlaying = true;
                }

                private void Pause_CanExecute(object sender, CanExecuteRoutedEventArgs e)
                {
                        e.CanExecute = mediaPlayerIsPlaying;
                }

                private void Pause_Executed(object sender, ExecutedRoutedEventArgs e)
                {
                        mePlayer.Pause();
                }

                private void Stop_CanExecute(object sender, CanExecuteRoutedEventArgs e)
                {
                        e.CanExecute = mediaPlayerIsPlaying;
                }

                private void Stop_Executed(object sender, ExecutedRoutedEventArgs e)
                {
                        mePlayer.Stop();
                        mediaPlayerIsPlaying = false;
                }

                private void sliProgress_DragStarted(object sender, DragStartedEventArgs e)
                {
                        userIsDraggingSlider = true;
                }

                private void sliProgress_DragCompleted(object sender, DragCompletedEventArgs e)
                {
                        userIsDraggingSlider = false;
                        mePlayer.Position = TimeSpan.FromSeconds(sliProgress.Value);
                }

                private void sliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
                {
                        lblProgressStatus.Text = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"hh\:mm\:ss");
                }

                private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
                {
                        mePlayer.Volume += (e.Delta > 0) ? 0.1 : -0.1;
                }

                private void LiveTabItem_GotFocus(object sender, RoutedEventArgs e)
                {
                this.ActiveMode = Mode.Live;    
                }

                private void TaggingTabItem_GotFocus(object sender, RoutedEventArgs e)
                    {
                    this.ActiveMode = Mode.Tagging;
                    
                    //InitTaggingStuff();
                    }

                private void TrainingTabItem_GotFocus(object sender, RoutedEventArgs e)
                    {
                    this.ActiveMode = Mode.Training;
                    }

                

    }
}
