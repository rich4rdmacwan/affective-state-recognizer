﻿using System;

namespace ConsoleApplication1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
                       
            classifyTest();
            
            Console.ReadLine();
        }
        const int percentSplit = 90;
        public static void classifyTest()
        {
            try
            {
                weka.core.Instances insts = new weka.core.Instances(new java.io.FileReader("ml17.arff"));
                insts.setClassIndex(insts.numAttributes() - 1);

                ///**************** Navisbayes ***************
                //weka.classifiers.Classifier cl = new weka.classifiers.bayes.NaiveBayes();

                ///*************** Randm Foresrt ************
                //weka.classifiers.Classifier cl = new weka.classifiers.trees.RandomForest();
                //weka.classifiers.trees.RandomForest cl = new weka.classifiers.trees.RandomForest();
               // cl.setNumTrees(100);
                //cl.setNumFeatures(0);

                ///*************** AdaBoost ************
                weka.classifiers.Classifier cl = new weka.classifiers.meta.AdaBoostM1();

                             
               
                Console.WriteLine("Performing " + percentSplit + "% split evaluation.");
                

                //randomize the order of the instances in the dataset.
                weka.filters.Filter myRandom = new weka.filters.unsupervised.instance.Randomize();
                myRandom.setInputFormat(insts);
                insts = weka.filters.Filter.useFilter(insts, myRandom);

                //double ff = cl.getNumFeatures();
                //Console.WriteLine("num of feature  " + ff.ToString());

                int trainSize = insts.numInstances() * percentSplit / 100;
                int testSize = insts.numInstances() - trainSize;
                weka.core.Instances train = new weka.core.Instances(insts, 0, trainSize);

                cl.buildClassifier(train);
                
                int numCorrect = 0;
                for (int i = trainSize; i < insts.numInstances(); i++)
                {
                    //Make classfication for the test in put instance
                    weka.core.Instance currentInst = insts.instance(i);
                    double predictedClass = cl.classifyInstance(currentInst);

                    // Get the true class label from the instance's own classIndex.
                    String trueClassLabel = insts.instance(i).toString(insts.instance(i).classIndex());
                        

                    // Get the prediction probability distribution.
                    double[] predictionDistribution =  cl.distributionForInstance(insts.instance(i));
                    foreach ( double d in predictionDistribution)
                    {
                        Console.WriteLine("probablity " + d.ToString());
                    }
                   
                    // Get the predicted class label from the predictionIndex.
                    String predictedClassLabel = insts.instance(i).classAttribute().value((int)predictedClass);

                   // Print out the true label, predicted label, and the distribution.
                    Console.WriteLine("======> True class = " + trueClassLabel +  "   Pridicted = "+ predictedClassLabel);
                       

                    if (predictedClass == insts.instance(i).classValue())
                        numCorrect++;
                }
                Console.WriteLine(numCorrect + " out of " + testSize + " correct (" +
                           (double)((double)numCorrect / (double)testSize * 100.0) + "%)");

                //Console.WriteLine("Clssifier Description " + cl.toString());

            }
            catch (java.lang.Exception ex)
            {
                ex.printStackTrace();
            }
        }
 
    }
}
