﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class MainClass
    {
        public static void Main(string[] args)
        {

            classifyTest();

            Console.ReadLine();
        
            }

        public static void classifyTest()
        {
            try
            {
                List<string> Mousemoved = new List<string>() { "Unknown", "Yes", "No", "Maybe" };
                List<string> Righteye = new List<string>() { "Unknown", "Maybe", "No", "Yes" };
                List<string> Lookingaway = new List<string>() { "Yes", "Maybe", "No" };
                List<string> Engaged = new List<string>() { "No", "Maybe", "Yes" };

                weka.core.Instances insts = new weka.core.Instances(new java.io.FileReader("../../Train_Data.arff"));
                if (insts.classIndex() == -1)

                    insts.setClassIndex(insts.numAttributes() - 1);

                ///*************** Randm Foresrt ************

              //  weka.classifiers.trees.RandomForest cl = new weka.classifiers.trees.RandomForest();
                weka.classifiers.functions.MultilayerPerceptron NN = new weka.classifiers.functions.MultilayerPerceptron();
                weka.classifiers.meta.AdaBoostM1 cl = new weka.classifiers.meta.AdaBoostM1();
                cl.setClassifier(NN);

                Console.WriteLine("building the classfier...");
                cl.buildClassifier(insts);
                Console.WriteLine("Start testing...");


                // serialize model
                weka.core.SerializationHelper.write("../../NNAdaboost.model", cl);

                // deserialize model
                //java.io.ObjectInputStream ois = new java.io.ObjectInputStream(new java.io.FileInputStream("../../RF.model"));
                //weka.classifiers.Classifier Final_classifier = (weka.classifiers.Classifier)ois.readObject();
                //ois.close();

                // testing

                string datain = "11,-4,0,No,No,No,No,Yes,-0.199718,0.203213,-0.185446,0.205482,-0.214384,-0.342218,-0.068524,-0.556861,-0.187796,-0.070721,0.013234,-0.257677,-0.012331,-0.156959,-0.603372,-0.213148,-0.566231,0,0.109,0.015,0.425,0.233,0.003,0.357,0.35,0.772,0.254,0,0.05,0,0,0,0.639,0.547";

                char sp = ',';
                string[] data = datain.Split(sp);


                java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.FileReader("Train_Data.arff"));
                weka.core.converters.ArffLoader.ArffReader arff1 = new weka.core.converters.ArffLoader.ArffReader(reader);

                // Create an empty training set
                weka.core.Instances TestBucket = new weka.core.Instances(arff1.getStructure(), 1);

                TestBucket.setClassIndex(TestBucket.numAttributes() - 1);
                weka.core.DenseInstance iExample1 = new weka.core.DenseInstance(data.Length);

                for (int i = 0; i < data.Length; i++)
                {
                    double x;
                    bool check = double.TryParse(data[i], out x);
                    Console.WriteLine("the currrent token: " + data[i]);
                    if (check == true)
                    {
                        iExample1.setValue(i, double.Parse(data[i]));
                    }

                    else
                    {
                        switch (i)
                        {
                            case 3:
                                foreach (string n in Mousemoved)
                                {
                                    if (n == data[i]) // Will match once
                                        iExample1.setValueSparse(i, (double)Mousemoved.IndexOf(n));
                                }
                                break;
                            case 4:
                                foreach (string n in Mousemoved)
                                {
                                    if (n == data[i]) // Will match once
                                        iExample1.setValueSparse(i, (double)Mousemoved.IndexOf(n));
                                }
                                break;
                            case 5:
                                foreach (string n in Righteye)
                                {
                                    if (n == data[i]) // Will match once
                                        iExample1.setValueSparse(i, (double)Mousemoved.IndexOf(n));

                                }
                                break;
                            case 6:
                                foreach (string n in Lookingaway)
                                {
                                    if (n == data[i]) // Will match once
                                        iExample1.setValueSparse(i, (double)Mousemoved.IndexOf(n));

                                }
                                break;
                            case 7:
                                foreach (string n in Engaged)
                                {
                                    if (n == data[i]) // Will match once
                                        iExample1.setValueSparse(i, (double)Mousemoved.IndexOf(n));

                                }
                                break;

                        }

                    }
                }
                Console.WriteLine("Num  Instances before: " + TestBucket.numInstances().ToString());
                TestBucket.add(iExample1);
                TestBucket.clear();
                Console.WriteLine("Num  Instances before: " + TestBucket.numInstances().ToString());
                Console.WriteLine("the instance is: " + iExample1.value(2).ToString());

                double classif = cl.classifyInstance(TestBucket.firstInstance());

                // Get the predicted class label from the predictionIndex.
                string predictedClassLabel = TestBucket.lastInstance().classAttribute().value((int)classif);

                // Print out the true label, predicted label, and the distribution.
                Console.WriteLine("======> Pridicted = " + predictedClassLabel);

                Console.WriteLine("Result " + classif.ToString());
                //get the prediction probability distribution.
                double[] predictiondistribution = cl.distributionForInstance(TestBucket.lastInstance());

                foreach (double d in predictiondistribution)
                {
                    Console.WriteLine("probablity " + d.ToString());
                }

            }
            catch (java.lang.Exception ex)
            {
                ex.printStackTrace();
            }


        }

    }
}
























