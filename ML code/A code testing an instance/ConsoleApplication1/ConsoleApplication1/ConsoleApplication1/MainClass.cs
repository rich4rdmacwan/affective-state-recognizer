﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ConsoleApplication1
{
    class MainClass
    {
        public static void Main(string[] args)
        {

            classifyTest();

            Console.ReadLine();
        }

        public static void classifyTest()
        {
            try
            {
                //Instantiate Array of nominal attribute indices. We use a map for each nominal value.
                //nominalAttIndices[0] corresponds to MouthMoved. We populate the comma seperated values 
                //in the hashtable and give them indices. Then while generating the instance, we use these hashtables to get the indices of the nominal values
            Hashtable test = new Hashtable();
            string ksey = "Key";
            test.Add(ksey, "Value");
            Console.WriteLine(test[ksey]);
                Hashtable[] nominalAttIndices = new Hashtable[5];
                for (int i = 0; i < nominalAttIndices.Length; i++)
                    {
                    nominalAttIndices[i] = new Hashtable();
                    }        
                
                //MouthMoved and LeftEyeClosed values
                for (int i = 0; i < 2; i++) { 
                nominalAttIndices[i].Add("Unknown", 0);
                nominalAttIndices[i].Add("Yes", 1);
                nominalAttIndices[i].Add("No", 2);
                nominalAttIndices[i].Add("Maybe", 3);
                }
                //RightEyeClosed
                nominalAttIndices[2].Add("Unknown", 0);
                nominalAttIndices[2].Add("Maybe", 1);
                nominalAttIndices[2].Add("No", 2);
                nominalAttIndices[2].Add("Yes", 3);

                //LookingAway
                nominalAttIndices[3].Add("Yes", 0);
                nominalAttIndices[3].Add("Maybe",1);
                nominalAttIndices[3].Add("No", 2);

                //Engaged
                nominalAttIndices[4].Add("No", 0);
                nominalAttIndices[4].Add("Maybe", 1);
                nominalAttIndices[4].Add("Yes", 2);
                

                
                
                //If after this, we do not have all the values then the file may be corrupt. Throw an exception
                weka.core.Instances insts = new weka.core.Instances(new java.io.FileReader("../../Train_Data.arff"));
                
                if (insts.classIndex() == -1)
                    insts.setClassIndex(insts.numAttributes() - 1);

                ///**************** Navisbayes ***************
                //weka.classifiers.Classifier cl = new weka.classifiers.bayes.NaiveBayes();

                ///*************** Randm Foresrt ************
                // weka.classifiers.Classifier cl = new weka.classifiers.trees.RandomForest();
                //weka.classifiers.trees.RandomForest cl = new weka.classifiers.trees.RandomForest();
                //cl.setNumTrees(50);
                //cl.setNumFeatures(10);

                ///*************** AdaBoost ************
                //weka.classifiers.Classifier cl = new weka.classifiers.meta.AdaBoostM1();

                // weka.classifiers.Classifier cl = new weka.classifiers.trees.J48(); 
                // weka.classifiers.functions.MultilayerPerceptron cl = new weka.classifiers.functions.MultilayerPerceptron();

                // the next three lines will make the cross validation test
                //weka.classifiers.Evaluation eval = new weka.classifiers.Evaluation(insts);
                //eval.crossValidateModel(cl, insts, 10, new java.util.Random());
                //Console.WriteLine(eval.toSummaryString());
                //Console.WriteLine("Initiate learning...");
                //cl.buildClassifier(insts);
                //Console.WriteLine("model trained");

                // serialize model
                //weka.core.SerializationHelper.write("../../RF1.model", cl);

                // deserialize model
                TimeSpan begin = Process.GetCurrentProcess().TotalProcessorTime;
                java.io.ObjectInputStream ois = new java.io.ObjectInputStream(new java.io.FileInputStream("../../RF.model"));
             
                
                weka.classifiers.Classifier cl= (weka.classifiers.Classifier)ois.readObject();
                ois.close();
                TimeSpan end = Process.GetCurrentProcess().TotalProcessorTime;
                Console.WriteLine("Time to read java object: " + (end - begin).TotalMilliseconds + " ms");
                

                
                // testing
                // string datain = "4.9,3.1,1.5,.1";
                string datain = "7 , 0 , -3 , Maybe , No , No , No , Yes , -0.007722519 , 0.4321168 , -0.001700956 , 0.4160856 , -0.005599804 , -0.1460819 , 0.1243514 , -0.3960602 , 0.04095127 , 0.09960286 , 0.2160532 , -0.07616186 , 0.2256374 , -0.05019962 , -0.4069638 , -0.02362915 , -0.3567763 , 0.083 , 0.091 , 0.025 , -0.105 , 0.172 , 0.189 , 0.296 , 0.172 , 0.147 , 0.217 , 0.065 , 0.017 , 0.017 , 0.016 , 0.042 , -0.108 , 0.227";

                char sp = ',';
                string[] data = datain.Split(sp);


                java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.FileReader("../../training.arff"));
                weka.core.converters.ArffLoader.ArffReader arff1 = new weka.core.converters.ArffLoader.ArffReader(reader);
                
                // Create an empty training set
                weka.core.Instances TestBucket = new weka.core.Instances(arff1.getStructure(), 1);
                
                TestBucket.setClassIndex(TestBucket.numAttributes() - 1);
                weka.core.DenseInstance iExample1 = new weka.core.DenseInstance(data.Length);
                
                
                for (int i = 0; i < data.Length; i++)
                {
                    double x;
                    bool check = double.TryParse(data[i], out x);
                //    Console.WriteLine("the currrent token: " + data[i]);
                    if (check == true)
                    {
                        iExample1.setValue(i, double.Parse(data[i]));
                    }

                    else
                    {
                        string key = data[i].ToString().Trim();
                        Hashtable ht = nominalAttIndices[i-3];
                        int index = (int)ht[key];
                        iExample1.setValue(i, index);
                    }

                }

                Console.WriteLine("Num  Instances before: " + TestBucket.numInstances().ToString());
                TestBucket.add(iExample1);
                Console.WriteLine("Num  Instances before: " + TestBucket.numInstances().ToString());
                weka.core.Instance instance = TestBucket.lastInstance();
                weka.core.Instance instance1 = TestBucket.get(0);
                weka.core.Instance instance2 = TestBucket.instance(0);
                double classif = cl.classifyInstance(TestBucket.lastInstance());

                Console.WriteLine("Result " + classif.ToString());
                
                
                //get the prediction probability distribution.
                double[] predictiondistribution = cl.distributionForInstance(TestBucket.lastInstance());

                foreach (double d in predictiondistribution)
                {
                    Console.WriteLine("probablity " + d.ToString());
                }

            }
            catch (java.lang.Exception ex)
            {
                ex.printStackTrace();
            }
        }

    }
}

